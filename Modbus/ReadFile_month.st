(*
 * Feb 12, 2024
*)
FUNCTION_BLOCK ReadFile_month
VAR_INPUT

END_VAR

VAR_OUTPUT

END_VAR

VAR
	len1:INT;
	len2:INT;
	Send :BOOL;
	PauseTrig:BOOL;
	Pause:TON;
	i:INT;
	Build:BOOL;
	aux1:BOOL;
	auxINT:INT;
	init:BOOL;
	CardId: DINT;
	Chan: DINT;
	Cmd: DWORD;			(* Card/driver command (specific) *)
	ArgVal: DWORD;
	PduSend :ARRAY[0..8]OF BYTE:=20,7,6,0,91,0,0,0,0;(* Function Code Hex=14, Byte CountM, Reference Type, File Number Hi, File Number Lo*) 
	PduRcv :ARRAY[0..255]OF BYTE;
	RcvLen	:INT;
	Command:DINT;
	SendTrig: R_TRIG; 
	SendTrig2: R_TRIG;
	BuildTrig: R_TRIG; 
	Trig_3: R_TRIG;
	ResetTrig: R_TRIG; 
	MBM_doCmd : Mio_DoCmd8_NB;
	auxFloat	:REAL;   
	auxArray	:ARRAY[0..800]OF BYTE;	
	PduSendLEN :DWORD:=9;
	auxDaT:DT;
END_VAR
SendTrig(S1 := read101M OR read102M OR read401M OR read402M OR read403M, Q0 => );
IF SendTrig.Q0 THEN
	Send:=TRUE;	
	CountM:=1;
	IF read101M THEN
		CardNum_M:=101;
		read101M:=FALSE;	
	END_IF
	IF read102M THEN
		CardNum_M:=102;
		read102M:=FALSE; 
	END_IF
	IF read401M THEN
		CardNum_M:=401;
		read401M:=FALSE;
	END_IF
	IF read402M THEN
		CardNum_M:=402;
		read402M:=FALSE;
	END_IF
	IF read403M THEN
		CardNum_M:=403;
		read403M:=FALSE;
	END_IF
END_IF

CardId:=Mio_GetIdToCard(CardNum := CardNum_M);

IF CountM=1 AND Send THEN
	PduSend[5] := 0;  	(* Record number Hi *) 
	PduSend[6] := 3;	(* Record number Lo *) 
   	PduSend[7] := 00;  	(* Record Length Hi *) 
   	PduSend[8] := 78;   (* Record Length lo *) 
   	RcvLen:=254;		(*RcvLen nieje len sp�tn� inform�cia o d�ke prijatej spr�vy ale zarove� parameter ur�eje maxim�lnu ve�kos�  *)
END_IF

IF CountM=2 AND Send THEN
	PduSend[5] := 0;  	(* Record number Hi *) 
	PduSend[6] := 81;	(* Record number Lo *) 
   	PduSend[7] := 00;  	(* Record Length Hi *) 
   	PduSend[8] := 72;   (* Record Length lo *) 
   	RcvLen:=254;
   	
END_IF

IF Send THEN
	Command:=Mio_DoCmd4(
		CardId := CardId,
		Chan := 0,
		Cmd := MIO_CMD_SENDMBMESSAGE,
		ArgVal1 :=  ADR(PduSend),
		ArgVal2 := PduSendLEN,
		ArgVal3 := ADR(PduRcv),
		ArgVal4 := ADR(RcvLen)
	);
END_IF

(**)
IF CountM=2 THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+39)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+39)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+39)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+39)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(HystMonths.Months[13]),
		Src := ADR(auxArray[160]),
		Len := RcvLen-4
	);
	CountM:=0;
	Send:=FALSE;
END_IF
(**)
IF CountM=1 THEN(**) 
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[(i*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[(i*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[(i*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[(i*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(HystMonths.Months[0]),
		Src := ADR(auxArray[4]),
		Len := RcvLen-4
	);
	CountM:=2;
END_IF
END_FUNCTION_BLOCK
