(*
 * Feb 12, 2024
*)
FUNCTION_BLOCK ReadFile2
VAR_INPUT

END_VAR

VAR_OUTPUT

END_VAR

VAR
	Counter:INT;
	Send :BOOL;
	PauseTrig:BOOL;
	Pause:TON;
	i:INT;
	Build:BOOL;
	aux1:BOOL;
	auxINT:INT;
	init:BOOL;
	CardId: DINT;
	Chan: DINT;
	Cmd: DWORD;			(* Card/driver command (specific) *)
	ArgVal: DWORD;
	PduSend :ARRAY[0..8]OF BYTE:=20,7,6,0,90,0,0,0,0;(* Function Code Hex=14, Byte Count, Reference Type, File Number Hi, File Number Lo*) 
	PduRcv :ARRAY[0..255]OF BYTE;
	RcvLen	:INT;
	Command:DINT;
	SendTrig: R_TRIG; 
	SendTrig2: R_TRIG;
	BuildTrig: F_TRIG; 
	Trig_3: R_TRIG;
	ResetTrig: R_TRIG; 
	MBM_doCmd : Mio_DoCmd8_NB;
	auxFloat	:REAL;   
	auxArray	:ARRAY[0..800]OF BYTE;
	Historia	:	PAC_Hist_Days;
	PduSendLEN :DWORD:=9;
	auxDaT:DT;
	num:INT;
END_VAR


CardId:=Mio_GetIdToCard(CardNum := num);
IF Send THEN
	RcvLen:=254;	(*RcvLen nieje len sp�tn� inform�cia o d�ke prijatej spr�vy ale zarove� parameter ur�eje maxim�lnu ve�kos�  *)
	Command:=Mio_DoCmd4(
		CardId := CardId,
		Chan := 0,
		Cmd := MIO_CMD_SENDMBMESSAGE,
		ArgVal1 :=  ADR(PduSend),
		ArgVal2 := PduSendLEN,
		ArgVal3 := ADR(PduRcv),
		ArgVal4 := ADR(RcvLen)
	);
	Send:=FALSE;
END_IF

IF Counter=4 AND Build THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+138)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+138)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+138)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+138)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(Historia.Days[46]),
		Src := ADR(auxArray[556]),
		Len := RcvLen-4
	);
	Build:=FALSE;
END_IF
(**)
IF Counter=3 AND Build THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+93)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+93)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+93)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+93)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(Historia.Days[31]),
		Src := ADR(auxArray[376]),
		Len := RcvLen-4
	);
	Build:=FALSE;
END_IF
(**)
IF Counter=2 AND Build THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+48)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+48)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+48)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+48)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(Historia.Days[16]),
		Src := ADR(auxArray[196]),
		Len := RcvLen-4
	);
	Build:=FALSE;
END_IF
(**)
IF Counter=1 AND Build THEN(**) 
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[(i*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[(i*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[(i*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[(i*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(Historia.Days[0]),
		Src := ADR(auxArray[4]),
		Len := RcvLen-4
	);
	Build:=FALSE;	
END_IF
END_FUNCTION_BLOCK
