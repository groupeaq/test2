(*
 * Apr 3, 2024
*)
FUNCTION_BLOCK Read_03
VAR_INPUT
Reg_1	:BOOL;
Reg_2	:BOOL;
Reg_3	:BOOL;
Reg_4	:BOOL;
Reg_5	:BOOL;
END_VAR

VAR_OUTPUT
Register_1	:REAL;
Register_2	:REAL;
Register_3	:REAL;
Register_4	:REAL;
Register_5	:REAL;
END_VAR

VAR
	Send 			:BOOL;
	x				:INT;
	i				:INT;
	CardId			:DINT;
	Chan			:DINT:=0;
	Cmd				:DWORD;			(* Card/driver command (specific) *)
	
	StartAdd		:UINT;
	nRegisters		:UINT;
	ROL_StartAdd	:UINT;
	ROL_nRegisters	:UINT;
	
	ArgVal			:DWORD;
	PduSend 		:ARRAY[0..4]OF BYTE:=3,0,0,0,0;(* Function Code Hex=03, Starting address Hi, Starting address Lo, No. of registers Hi, No. of registers Lo*) 
	PduRcv 			:ARRAY[0..255]OF BYTE;
	RcvLen			:INT;
	Command			:DINT;
	SendTrig		:R_TRIG; 
	SendTrig2		:R_TRIG;
	BuildTrig		:R_TRIG; 
	Trig_3			:R_TRIG;
	ResetTrig		:R_TRIG; 
	MBM_doCmd 		:Mio_DoCmd8_NB;
	auxFloat		:REAL;   
	auxArray		:ARRAY[0..800]OF BYTE;	
	PduSendLEN	 	:DWORD:=5;
END_VAR


CardId:=Mio_GetIdToCard(CardNum := CardNum);


IF Send THEN

	ROL_nRegisters:= ROL(nRegisters, x);
	ROL_StartAdd:= ROL(StartAdd, x);
	Mem_Copy(
		Dst := ADR(PduSend[1]),
		Src := ADR(ROL_StartAdd),
		Len := 2
	);
	Mem_Copy(
		Dst := ADR(PduSend[3]),
		Src := ADR(ROL_nRegisters),
		Len := 2
	);
	Command:=Mio_DoCmd4(
		CardId := CardId,
		Chan := Chan,
		Cmd := MIO_CMD_SENDMBMESSAGE,
		ArgVal1 :=  ADR(PduSend),
		ArgVal2 := PduSendLEN,
		ArgVal3 := ADR(PduRcv),
		ArgVal4 := ADR(RcvLen)
	);
	Send:=FALSE;
END_IF

(**)

(* FOR i := 1 TO (nRegisters) - 1 DO *)
(*     auxArray[(i*4) + 0] := PduRcv[(i*4) + 3]; *)
(*     auxArray[(i*4) + 1] := PduRcv[(i*4) + 2]; *)
(*     auxArray[(i*4) + 2] := PduRcv[(i*4) + 1]; *)
(*     auxArray[(i*4) + 3] := PduRcv[(i*4) + 0]; *)
(* END_FOR; *)
(* Mem_Copy( *)
(*	Dst := ADR(HystDays.Days[0]), *)
(*	Src := ADR(auxArray[4]), *)
(*	Len := RcvLen-4 *)
(* ); *)

END_FUNCTION_BLOCK
