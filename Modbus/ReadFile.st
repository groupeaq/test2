(*
 * Feb 12, 2024
*)
FUNCTION_BLOCK ReadFile
VAR_INPUT

END_VAR

VAR_OUTPUT

END_VAR

VAR
	len1:INT;
	len2:INT;
	Send :BOOL;
	PauseTrig:BOOL;
	Pause:TON;
	i:INT;
	Build:BOOL;
	aux1:BOOL;
	auxINT:INT;
	init:BOOL;
	CardId: DINT;
	Chan: DINT;
	Cmd: DWORD;			(* Card/driver command (specific) *)
	ArgVal: DWORD;
	PduSend :ARRAY[0..8]OF BYTE:=20,7,6,0,90,0,0,0,0;(* Function Code Hex=14, Byte Count, Reference Type, File Number Hi, File Number Lo*) 
	PduRcv :ARRAY[0..255]OF BYTE;
	RcvLen	:INT;
	Command:DINT;
	SendTrig: R_TRIG; 
	SendTrig2: R_TRIG;
	BuildTrig: R_TRIG; 
	Trig_3: R_TRIG;
	ResetTrig: R_TRIG; 
	MBM_doCmd : Mio_DoCmd8_NB;
	auxFloat	:REAL;   
	auxArray	:ARRAY[0..800]OF BYTE;	
	PduSendLEN :DWORD:=9;
	auxDaT:DT;
END_VAR
SendTrig(S1 := read101 OR read102 OR read401 OR read402 OR read403, Q0 => );
IF SendTrig.Q0 THEN
	Send:=TRUE;	
	Count:=1;
	IF read101 THEN
		CardNum:=101;
		read101:=FALSE;	
	END_IF
	IF read102 THEN
		CardNum:=102;
		read102:=FALSE; 
	END_IF
	IF read401 THEN
		CardNum:=401;
		read401:=FALSE;
	END_IF
	IF read402 THEN
		CardNum:=402;
		read402:=FALSE;
	END_IF
	IF read403 THEN
		CardNum:=403;
		read403:=FALSE;
	END_IF
END_IF

CardId:=Mio_GetIdToCard(CardNum := CardNum);

IF Count=1 AND Send THEN
	PduSend[5] := 0;  	(* Record number Hi *) 
	PduSend[6] := 3;	(* Record number Lo *) 
   	PduSend[7] := 00;  	(* Record Length Hi *) 
   	PduSend[8] := 96;   (* Record Length lo *) 
   	RcvLen:=254;		(*RcvLen nieje len sp�tn� inform�cia o d�ke prijatej spr�vy ale zarove� parameter ur�eje maxim�lnu ve�kos�  *)
END_IF

IF Count=2 AND Send THEN
	PduSend[5] := 0;  	(* Record number Hi *) 
	PduSend[6] := 99;	(* Record number Lo *) 
   	PduSend[7] := 00;  	(* Record Length Hi *) 
   	PduSend[8] := 90;   (* Record Length lo *) 
   	RcvLen:=254;
   	
END_IF

IF Count=3 AND Send THEN
	PduSend[5] := 0;  	(* Record number Hi *) 
	PduSend[6] := 189;	(* Record number Lo *) 
   	PduSend[7] := 00;  	(* Record Length Hi *) 
   	PduSend[8] := 90;   (* Record Length lo *)
   	RcvLen:=254; 
END_IF

IF Count=4 AND Send THEN
	PduSend[5] := 1;  	(* Record number Hi *) 
	PduSend[6] := 23;	(* Record number Lo *) 
   	PduSend[7] := 00;  	(* Record Length Hi *) 
   	PduSend[8] := 90;   (* Record Length lo *)
   	RcvLen:=254;
END_IF

IF Send THEN
	Command:=Mio_DoCmd4(
		CardId := CardId,
		Chan := 0,
		Cmd := MIO_CMD_SENDMBMESSAGE,
		ArgVal1 :=  ADR(PduSend),
		ArgVal2 := PduSendLEN,
		ArgVal3 := ADR(PduRcv),
		ArgVal4 := ADR(RcvLen)
	);
END_IF

IF Count=4 THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+138)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+138)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+138)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+138)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(HystDays.Days[46]),
		Src := ADR(auxArray[556]),
		Len := RcvLen-4
	);
	Count:=0;
	Send:=FALSE;
END_IF
(**)
IF Count=3 THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+93)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+93)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+93)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+93)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(HystDays.Days[31]),
		Src := ADR(auxArray[376]),
		Len := RcvLen-4
	);
	Count:=4;
END_IF
(**)
IF Count=2 THEN (**)
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[((i+48)*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[((i+48)*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[((i+48)*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[((i+48)*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(HystDays.Days[16]),
		Src := ADR(auxArray[196]),
		Len := RcvLen-4
	);
	Count:=3;
END_IF
(**)
IF Count=1 THEN(**) 
	FOR i := 1 TO ((RcvLen-4) / 4) - 1 DO
	    auxArray[(i*4) + 0] := PduRcv[(i*4) + 3];
	    auxArray[(i*4) + 1] := PduRcv[(i*4) + 2];
	    auxArray[(i*4) + 2] := PduRcv[(i*4) + 1];
	    auxArray[(i*4) + 3] := PduRcv[(i*4) + 0];
	END_FOR;
	Mem_Copy(
		Dst := ADR(HystDays.Days[0]),
		Src := ADR(auxArray[4]),
		Len := RcvLen-4
	);
	Count:=2;
END_IF
END_FUNCTION_BLOCK
