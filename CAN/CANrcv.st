(*
 * Feb 13, 2024
*)
FUNCTION_BLOCK CANrcv
VAR_INPUT

END_VAR

VAR_OUTPUT

	Inv_timestamp		:DT;
	Inv_SOC				:REAL;
	Inv_Voltage			:REAL;
END_VAR

VAR
	x:INT;
	aux					:BOOL;
	auxWord				:WORD;
	auxDword			:DWORD;
	Inv_timestamp2		:DT;
	auxWord2			:WORD;
	auxword3			:WORD;
	command				:DINT;
	CardId				:DINT;
	DataRcv				:ARRAY[0..30]OF BYTE;
	Rcv					:CAN_Rcv;
	ID					:DINT;
	TS					:TIME;
	auxArray			:ARRAY[0..3] OF BYTE;
	cmd:DINT:=5003;
END_VAR
CardId:=Mio_GetIdToCard(CardNum := CAN_CardNum);
IF aux THEN
command:=Mio_DoCmd(
	CardId := CardId,
	Chan := 0,
	Cmd := cmd,
	ArgVal := ADR(Rcv)
);
END_IF
IF Rcv.ID=16#151 AND NOT CAN_init THEN
	BYD_init();
END_IF
IF Rcv.ID=16#111 THEN
	Mem_Copy(ADR(auxArray[3]), ADR(Rcv.b0),1);
	Mem_Copy(ADR(auxArray[2]), ADR(Rcv.b1),1);
	Mem_Copy(ADR(auxArray[1]), ADR(Rcv.b2),1);
	Mem_Copy(ADR(auxArray[0]), ADR(Rcv.b3),1);
	Mem_Copy(ADR(Inv_timestamp), ADR(auxArray[0]),4);
END_IF
IF Rcv.ID=16#D1 THEN
	Mem_Copy(ADR(auxWord), ADR(Rcv.b0),2);
	auxWord:= ROL(auxWord,8);
	Inv_SOC:= (WORD_TO_REAL(auxWord))*0.1;
END_IF
IF Rcv.ID=16#91 THEN
	Mem_Copy(ADR(auxWord), ADR(Rcv.b0),2);
	auxWord:= ROL(auxWord,8);
	Inv_Voltage:= (WORD_TO_REAL(auxWord))*0.1;
END_IF
END_FUNCTION_BLOCK
