(*
 * Feb 21, 2024
*)
FUNCTION_BLOCK CAN_reset
VAR_INPUT

END_VAR

VAR_OUTPUT

END_VAR

VAR
	Trig:BOOL;
	CardID:DINT;
	cmd:DINT;
	command:DINT;
END_VAR
IF Trig THEN
CardID:=Mio_GetIdToCard(CardNum := CAN_CardNum);
command:=Mio_DoCmd(
			CardId := CardID,
			Chan := 0,
			Cmd := cmd,
			ArgVal := 0
		);
		Trig:=FALSE;
END_IF		
	
END_FUNCTION_BLOCK
