(*
 * Feb 19, 2024
*)
FUNCTION_BLOCK CAN_BYD_HVS

VAR CONSTANT
	MaxVoltage		:REAL:=350.4;
	MinVoltage		:REAL:=240.0;
	MaxChaCurrent	:REAL:=25.6;
	MaxDischaCurrent:REAL:=25.6;
END_VAR

VAR_INPUT
	init			:BOOL;
	Bat_SoC			:REAL;
	Bat_SoH			:REAL;
	Bat_MaxCH_P		:REAL;
	Bat_MaxDiscH_P	:REAL;
	Bat_U			:REAL;
	Bat_I			:REAL;
	Bat_T			:REAL;
	Bat_Tmax		:REAL;
	Bat_Tmin		:REAL;
END_VAR

VAR
	auxWord			:WORD;
	CardID			:DINT;
	Cmd				:DINT;
	Pulse2s			:BOOL;
	Pulse10s		:BOOL;
	Pulse60s		:BOOL;
	Send			:BOOL;
	initTrig		:R_TRIG;
	Timer2s			:TON;
	Timer10s		:TON;
	Timer60s		:TON;
	Pause2s			:TP;
	Pause10s		:TP;
	Pause60s		:TP;
	Trig2s			:F_TRIG;
	Trig10s			:F_TRIG;
	Trig60s			:F_TRIG;
	DataSend		:CAN_Send;

END_VAR

VAR_OUTPUT
END_VAR

CardID:=Mio_GetIdToCard(CardNum := CAN_CardNum);

initTrig(S1 := init, Q0 => );

IF initTrig.Q0 THEN
	Pulse2s:=TRUE;
	Pulse10s:=TRUE;
	Pulse60s:=TRUE;
END_IF

IF init THEN

	Pause2s(IN := NOT Pulse2s, PT := T#500ms);
	Trig2s(S1 := Pause2s.Q, Q0 => );
	IF Trig2s.Q0 THEN Pulse2s:=TRUE; END_IF
	Timer2s(IN := Pulse2s, PT := T#1s500ms);
	
	Pause10s(IN := NOT Pulse10s, PT := T#500ms);
	Trig10s(S1 := Pause10s.Q, Q0 => );	
	IF Trig10s.Q0 THEN Pulse10s:=TRUE; END_IF
	Timer10s(IN := Pulse10s, PT := T#9s500ms);
	
	Pause60s(IN := NOT Pulse60s, PT := T#500ms);
	Trig60s(S1 := Pause60s.Q, Q0 => );
	IF Trig60s.Q0 THEN Pulse60s:=TRUE; END_IF
	Timer60s(IN := Pulse60s, PT := T#59s500ms);

	IF Timer2s.Q THEN
		DataSend.ID:=16#110;
		DataSend.DLC:=16#88;
		auxWord:=REAL_TO_WORD(MaxVoltage*10);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b0), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(MinVoltage*10);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b2), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(MaxChaCurrent*10);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b4), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(MaxDischaCurrent*10);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b6), ADR(auxWord), 2);
		
		Cmd:=Mio_DoCmd(
			CardId := CardID,
			Chan := 0,
			Cmd := 5002,
			ArgVal := ADR(DataSend)
		);
	
		Pulse2s:=FALSE;
	END_IF

	IF  Timer10s.Q THEN
		DataSend.ID:=16#150;
		DataSend.DLC:=16#88;
		auxWord:=REAL_TO_WORD(Bat_SoC*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b0), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(Bat_SoH*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b2), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(Bat_MaxCH_P*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b4), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(Bat_MaxDiscH_P*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b6), ADR(auxWord), 2);
		
		Cmd:=Mio_DoCmd(
			CardId := CardID,
			Chan := 0,
			Cmd := 5002,
			ArgVal := ADR(DataSend)
		);
		
		DataSend.ID:=16#1D0;
		DataSend.DLC:=16#88;
		DataSend.b6:=16#0;
		DataSend.b7:=16#0;
		auxWord:=REAL_TO_WORD(Bat_U*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b0), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(Bat_I*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b2), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(Bat_T*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b4), ADR(auxWord), 2);
		
		Cmd:=Mio_DoCmd(
			CardId := CardID,
			Chan := 0,
			Cmd := 5002,
			ArgVal := ADR(DataSend)
		);
		
		DataSend.ID:=16#210;
		DataSend.DLC:=16#88;
		DataSend.b4:=16#0;
		DataSend.b5:=16#0;
		DataSend.b6:=16#0;
		DataSend.b7:=16#0;
		auxWord:=REAL_TO_WORD(Bat_Tmax*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b0), ADR(auxWord), 2);
		
		auxWord:=REAL_TO_WORD(Bat_Tmin*100);
		auxWord:=ROL(auxWord,8);
		Mem_Copy(ADR(DataSend.b2), ADR(auxWord), 2);
		
		Cmd:=Mio_DoCmd(
			CardId := CardID,
			Chan := 0,
			Cmd := 5002,
			ArgVal := ADR(DataSend)
		);
		Pulse10s:=FALSE;
	END_IF
	IF  Timer60s.Q THEN
		DataSend.ID:=16#190;
		DataSend.DLC:=16#88;
		DataSend.b0:=16#00;
		DataSend.b1:=16#00;
		DataSend.b2:=16#00;
		DataSend.b3:=16#0E;
		DataSend.b4:=16#00;
		DataSend.b5:=16#00;
		DataSend.b6:=16#00;
		DataSend.b7:=16#0A;
		Cmd:=Mio_DoCmd(
			CardId := CardID,
			Chan := 0,
			Cmd := 5002,
			ArgVal := ADR(DataSend)
		);
		Pulse60s:=FALSE;
	END_IF
END_IF
END_FUNCTION_BLOCK
