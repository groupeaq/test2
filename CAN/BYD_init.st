FUNCTION BYD_init : INT
VAR_INPUT

END_VAR

VAR
i:INT;
CardID:DINT;
command:DINT;
data:ARRAY[0..15] OF BYTE;
END_VAR
CardID:=Mio_GetIdToCard(CardNum := CAN_CardNum);
FOR i:=0 TO 6 DO
	Mem_Copy(ADR(data),ADR(CAN_InitData[i,0]),16);
	command:=Mio_DoCmd(
		CardId := CardID,
		Chan := 0,
		Cmd := 5002,
		ArgVal := ADR(data));
END_FOR
CAN_init:=TRUE;
END_FUNCTION