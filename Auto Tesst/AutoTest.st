(*
 * Apr 29, 2024
*)
FUNCTION_BLOCK AutoTest
VAR_INPUT
	Start		:BOOL;
	reset		:BOOL;
END_VAR

VAR_OUTPUT

END_VAR

VAR
	T1			:TOF;
	T2			:TOF;
	StartEdge	:R_TRIG;
	T1Edge		:F_TRIG;
	T2Edge		:F_TRIG;
	CountPlus	:INT;
	CountMinus	:INT;
	PlusMinus	:BOOL;
END_VAR
IF reset THEN
	CountPlus:=0;
	CountMinus:=0;
	AutoSetpoint:=0;
	RETURN;
END_IF
IF Start THEN
	StartEdge(S1 := Start, Q0 => );	
	
	T1(
		IN := StartEdge.Q0 OR T2Edge.Q0,
		PT := T#59s,
		Q => ,
		ET => 
	);
	T1Edge(S1 := T1.Q, Q0 => );
	
	T2(
		IN := T1Edge.Q0,
		PT := T#1s,
		Q => ,
		ET => 
	);
	T2Edge(S1 := T2.Q, Q0 => );
	
	IF (T2Edge.Q0 OR StartEdge.Q0) AND (CountPlus <=14) THEN
		AutoSetpoint:= AutoSetpoint+0.1;
		CountPlus:= CountPlus + 1;
	END_IF
	IF (T2Edge.Q0 OR StartEdge.Q0) AND (CountMinus <=14) AND CountPlus = 15 THEN
		AutoSetpoint:= AutoSetpoint-0.1;
		CountMinus:= CountMinus + 1;
	END_IF
END_IF
;
END_FUNCTION_BLOCK
