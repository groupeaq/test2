(*
 * Jan 31, 2024 OL
*)
FUNCTION_BLOCK Measurement_DC_SunS
VAR_INPUT
	in_1			:UINT;
	in_2			:UINT;
	RetainData		:Merania_RetainPart;
	scale			:INT;
END_VAR

VAR_OUTPUT
	OUT				:REAL;
	OUT_aux			:REAL;
	HA_act			:BOOL;
	HW_act			:BOOL;
	LW_act			:BOOL;
	LA_act			:BOOL;
	Error			:WORD;
END_VAR

VAR
	aux_err			:WORD;
	reset			:BOOL;
END_VAR
IF reset THEN
		Error:=0;
END_IF
IF RetainData.Scale <=0 THEN
	Error:=999;
	RETURN;
END_IF

IF in_1 = 0 AND in_2 > 0 THEN
	OUT_aux:= UINT_TO_REAL(in_2);
ELSIF	in_2=0 AND in_1 > 0 THEN
	OUT_aux:= UINT_TO_REAL(in_1);
ELSE
	Error:=998;
END_IF 

CASE scale OF
	 0:	OUT:= OUT_aux;
	-1:	OUT:= OUT_aux/10;
	-2:	OUT:= OUT_aux/100;
	-3:	OUT:= OUT_aux/1000;
	-4:	OUT:= OUT_aux/10000;
	-5:	OUT:= OUT_aux/100000;
	-6:	OUT:= OUT_aux/1000000;
	-7:	OUT:= OUT_aux/10000000;
	-8:	OUT:= OUT_aux/100000000;
ELSE
	Error:=997;
	RETURN;
END_CASE

IF 	OUT > RetainData.HW AND RetainData.HW_en 				THEN	 HW_act:=TRUE; 		END_IF

IF 	OUT > RetainData.HA AND RetainData.HA_en 				THEN	 HA_act:=TRUE; 		END_IF
	
IF 	OUT < RetainData.LW AND RetainData.LW_en 				THEN	 LW_act:=TRUE; 		END_IF

IF 	OUT < RetainData.LA AND RetainData.LA_en  			THEN	 LA_act:=TRUE; 		END_IF

IF 	OUT < (RetainData.HW+RetainData.Hys) 	OR NOT RetainData.HW_en 	THEN	 HW_act:=FALSE; 		END_IF

IF	OUT < (RetainData.HA+RetainData.Hys)	OR NOT RetainData.HA_en 	THEN	 HA_act:=FALSE; 	END_IF

IF 	OUT > (RetainData.LW+RetainData.Hys)	OR NOT RetainData.LW_en		THEN 	 LW_act:=FALSE;		END_IF

IF 	OUT > (RetainData.LA+RetainData.Hys) 	OR NOT RetainData.LA_en	THEN	 LA_act:=FALSE;		END_IF

END_FUNCTION_BLOCK
