(*
 * Jan 31, 2024 OL
*)
FUNCTION_BLOCK Measurement
VAR_INPUT
_DWORD			:DWORD;
_REAL			:REAL;
_LREAL			:LREAL;
_Status			:BOOL;
RetainData		:Merania_RetainPart;
scale			:INT;
END_VAR

VAR_OUTPUT
OUT				:REAL;
OUT_aux				:REAL;
HA_act			:BOOL;
HW_act			:BOOL;
LW_act			:BOOL;
LA_act			:BOOL;
Error			:WORD;
END_VAR

VAR
aux_err			:WORD;
END_VAR
	Error:=0;
IF RetainData.Scale <=0 THEN
	Error:=999;
	RETURN;
END_IF


CASE RetainData.format OF
	1:OUT_aux:= (DWORD_TO_REAL(_DWORD))/RetainData.Scale;
	2:OUT_aux:= (DWORD_TO_REAL(_DWORD))/RetainData.Scale;
	3:OUT_aux:= _REAL/RetainData.Scale;
	4:OUT_aux:= (LREAL_TO_REAL(_LREAL))/RetainData.Scale;
ELSE
	Error:=997;
	RETURN;
END_CASE

CASE scale OF
	 0:	OUT:= OUT_aux;
	-1:	OUT:= OUT_aux/10;
	-2:	OUT:= OUT_aux/100;
	-3:	OUT:= OUT_aux/1000;
	-4:	OUT:= OUT_aux/10000;
	-5:	OUT:= OUT_aux/100000;
ELSE
	Error:=996;
	RETURN;
END_CASE

IF 	OUT > RetainData.HW AND RetainData.HW_en 				THEN	 HW_act:=TRUE; 		END_IF

IF 	OUT > RetainData.HA AND RetainData.HA_en 				THEN	 HA_act:=TRUE; 		END_IF
	
IF 	OUT < RetainData.LW AND RetainData.LW_en 				THEN	 LW_act:=TRUE; 		END_IF

IF 	OUT < RetainData.LA AND RetainData.LA_en  			THEN	 LA_act:=TRUE; 		END_IF

IF 	OUT < (RetainData.HW+RetainData.Hys) 	OR NOT RetainData.HW_en 	THEN	 HW_act:=FALSE; 		END_IF

IF	OUT < (RetainData.HA+RetainData.Hys)	OR NOT RetainData.HA_en 	THEN	 HA_act:=FALSE; 	END_IF

IF 	OUT > (RetainData.LW+RetainData.Hys)	OR NOT RetainData.LW_en		THEN 	 LW_act:=FALSE;		END_IF

IF 	OUT > (RetainData.LA+RetainData.Hys) 	OR NOT RetainData.LA_en	THEN	 LA_act:=FALSE;		END_IF

END_FUNCTION_BLOCK
