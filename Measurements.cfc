<?xml version="1.0" encoding="UTF-8"?>
<pou xmlns="http://www.plcopen.org/xml/tc6_0201" name="Measurements" pouType="functionBlock">
    <interface>
        <localVars/>
        <addData>
            <data name="www.bachmann.at/plc/plcopenxml" handleUnknown="implementation">
                <textDeclaration>
                    <content>
(*
 * Jan 31, 2024
*)
FUNCTION_BLOCK Measurements
VAR_INPUT

END_VAR

VAR_OUTPUT

END_VAR

VAR
F_SOC		:Measurement;
F_P			:Measurement;
F_Q			:Measurement;
F_PF		:Measurement;
F_C			:Measurement;
F_I_AC		:Measurement;
F_U_aN		:Measurement;
F_U_bN		:Measurement;
F_U_cN		:Measurement;
F_I_DC		:Measurement_DC_SunS;
F_U_DC		:Measurement_DC_SunS;
(**)
R1_T		:Measurement;
R1_SOC		:Measurement;
R1_C		:Measurement;
R1_P		:Measurement;
R1_U		:Measurement;
R1_I		:Measurement;
(**)
R2_T		:Measurement;
R2_SOC		:Measurement;
R2_C		:Measurement;
R2_P		:Measurement;
R2_U		:Measurement;
R2_I		:Measurement;
END_VAR

					</content>
                </textDeclaration>
            </data>
        </addData>
    </interface>
    <body>
        <FBD>
            <block localId="1" width="140" height="160" typeName="Measurement" instanceName="F_SOC" executionOrderId="0">
                <position x="170" y="-100"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="2"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="3"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="2" height="20" width="140" negated="false">
                <position x="10" y="-80"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_ChaState</expression>
            </inVariable>
            <inVariable localId="3" height="20" width="140" negated="false">
                <position x="10" y="0"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_SOC_RD</expression>
            </inVariable>
            <block localId="4" width="140" height="160" typeName="Measurement" instanceName="F_P" executionOrderId="1">
                <position x="490" y="-100"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="5"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="6"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="7" width="140" height="160" typeName="Measurement" instanceName="F_Q" executionOrderId="2">
                <position x="810" y="-100"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="8"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="9"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="10" width="140" height="160" typeName="Measurement" instanceName="F_PF" executionOrderId="3">
                <position x="1130" y="-100"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="11"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="12"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="9" height="20" width="140" negated="false">
                <position x="650" y="0"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_Q_RD</expression>
            </inVariable>
            <inVariable localId="5" height="20" width="140" negated="false">
                <position x="330" y="-60"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_P</expression>
            </inVariable>
            <inVariable localId="12" height="20" width="140" negated="false">
                <position x="970" y="0"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_PF_RD</expression>
            </inVariable>
            <inVariable localId="11" height="20" width="140" negated="false">
                <position x="970" y="-60"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_PF</expression>
            </inVariable>
            <inVariable localId="6" height="20" width="140" negated="false">
                <position x="330" y="0"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_P_RD</expression>
            </inVariable>
            <inVariable localId="8" height="20" width="140" negated="false">
                <position x="650" y="-60"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_Q</expression>
            </inVariable>
            <comment localId="13" height="20" width="236">
                <position x="0" y="-170"/>
                <content>
                    <pre>Format: 1=WORD,2=DWORD,3=REAL,4=LREAL</pre>
                </content>
            </comment>
            <block localId="14" width="140" height="160" typeName="Measurement" instanceName="R1_T" executionOrderId="4">
                <position x="170" y="280"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="15"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="16"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="17" width="140" height="160" typeName="Measurement" instanceName="R1_SOC" executionOrderId="5">
                <position x="490" y="280"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="18"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="19"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="20" width="140" height="160" typeName="Measurement" instanceName="R1_C" executionOrderId="6">
                <position x="810" y="280"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="21"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="22" width="140" height="160" typeName="Measurement" instanceName="R1_P" executionOrderId="7">
                <position x="1130" y="280"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="23"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="24"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="25" width="140" height="160" typeName="Measurement" instanceName="R1_U" executionOrderId="8">
                <position x="1470" y="280"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="26"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="27" width="140" height="160" typeName="Measurement" instanceName="R1_I" executionOrderId="9">
                <position x="1770" y="280"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="28"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="29" width="140" height="160" typeName="Measurement" instanceName="R2_T" executionOrderId="10">
                <position x="170" y="490"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="31" width="140" height="160" typeName="Measurement" instanceName="R2_SOC" executionOrderId="11">
                <position x="490" y="490"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="32"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="33" width="140" height="160" typeName="Measurement" instanceName="R2_C" executionOrderId="12">
                <position x="810" y="490"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="34"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="35" width="140" height="160" typeName="Measurement" instanceName="R2_P" executionOrderId="13">
                <position x="1130" y="490"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="36"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="37" width="140" height="160" typeName="Measurement" instanceName="R2_U" executionOrderId="14">
                <position x="1450" y="490"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="38"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="39" width="140" height="160" typeName="Measurement" instanceName="R2_I" executionOrderId="15">
                <position x="1770" y="490"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="40"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="16" height="20" width="82" negated="false">
                <position x="70" y="380"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R1_T_RD</expression>
            </inVariable>
            <inVariable localId="19" height="20" width="94" negated="false">
                <position x="370" y="380"/>
                <connectionPointOut>
                    <relPosition x="94" y="10"/>
                </connectionPointOut>
                <expression>R1_SOC_RD</expression>
            </inVariable>
            <inVariable localId="21" height="20" width="82" negated="false">
                <position x="710" y="380"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R1_C_RD</expression>
            </inVariable>
            <inVariable localId="24" height="20" width="82" negated="false">
                <position x="1030" y="380"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R1_P_RD</expression>
            </inVariable>
            <inVariable localId="26" height="20" width="82" negated="false">
                <position x="1370" y="380"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R1_U_RD</expression>
            </inVariable>
            <inVariable localId="28" height="20" width="82" negated="false">
                <position x="1670" y="380"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R1_I_RD</expression>
            </inVariable>
            <inVariable localId="40" height="20" width="82" negated="false">
                <position x="1670" y="590"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R2_I_RD</expression>
            </inVariable>
            <inVariable localId="36" height="20" width="82" negated="false">
                <position x="1030" y="590"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R2_P_RD</expression>
            </inVariable>
            <inVariable localId="34" height="20" width="82" negated="false">
                <position x="710" y="590"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R2_C_RD</expression>
            </inVariable>
            <inVariable localId="30" height="20" width="82" negated="false">
                <position x="70" y="590"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R2_T_RD</expression>
            </inVariable>
            <inVariable localId="32" height="20" width="94" negated="false">
                <position x="370" y="590"/>
                <connectionPointOut>
                    <relPosition x="94" y="10"/>
                </connectionPointOut>
                <expression>R2_SOC_RD</expression>
            </inVariable>
            <inVariable localId="38" height="20" width="82" negated="false">
                <position x="1350" y="590"/>
                <connectionPointOut>
                    <relPosition x="82" y="10"/>
                </connectionPointOut>
                <expression>R2_U_RD</expression>
            </inVariable>
            <connector name="R1SOC" localId="41" height="20" width="70">
                <position x="200" y="-140"/>
                <connectionPointIn>
                    <relPosition x="0" y="10"/>
                    <connection refLocalId="2">
                        <position x="200" y="-130"/>
                        <position x="160" y="-130"/>
                        <position x="160" y="-70"/>
                        <position x="150" y="-70"/>
                    </connection>
                </connectionPointIn>
            </connector>
            <continuation name="R1SOC" localId="18" height="20" width="80">
                <position x="390" y="300"/>
                <connectionPointOut>
                    <relPosition x="80" y="10"/>
                </connectionPointOut>
            </continuation>
            <inVariable localId="15" height="20" width="124" negated="false">
                <position x="30" y="320"/>
                <connectionPointOut>
                    <relPosition x="124" y="10"/>
                </connectionPointOut>
                <expression>Fronius_TmpCab</expression>
            </inVariable>
            <block localId="42" width="140" height="160" typeName="Measurement" instanceName="F_C" executionOrderId="17">
                <position x="1470" y="-100"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="23" height="20" width="94" negated="false">
                <position x="1020" y="320"/>
                <connectionPointOut>
                    <relPosition x="94" y="10"/>
                </connectionPointOut>
                <expression>Fronius_P</expression>
            </inVariable>
            <inVariable localId="43" height="20" width="140" negated="false">
                <position x="10" y="120"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_I</expression>
            </inVariable>
            <inVariable localId="44" height="20" width="140" negated="false">
                <position x="10" y="180"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_I_AC_RD</expression>
            </inVariable>
            <block localId="45" width="140" height="160" typeName="Measurement" instanceName="F_I_AC" executionOrderId="18">
                <position x="170" y="80"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="43"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="44"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="46" height="20" width="140" negated="false">
                <position x="330" y="180"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_U_aN_RD</expression>
            </inVariable>
            <inVariable localId="47" height="20" width="140" negated="false">
                <position x="330" y="120"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_U_aN</expression>
            </inVariable>
            <block localId="48" width="140" height="160" typeName="Measurement" instanceName="F_U_aN" executionOrderId="19">
                <position x="490" y="80"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="47"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="46"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="49" height="20" width="140" negated="false">
                <position x="650" y="180"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_U_bN_RD</expression>
            </inVariable>
            <inVariable localId="50" height="20" width="140" negated="false">
                <position x="650" y="120"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_U_bN</expression>
            </inVariable>
            <block localId="51" width="140" height="160" typeName="Measurement" instanceName="F_U_bN" executionOrderId="20">
                <position x="810" y="80"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="50"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="49"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="52" height="20" width="140" negated="false">
                <position x="970" y="180"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_U_cN_RD</expression>
            </inVariable>
            <inVariable localId="53" height="20" width="140" negated="false">
                <position x="970" y="120"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_U_cN</expression>
            </inVariable>
            <block localId="54" width="140" height="160" typeName="Measurement" instanceName="F_U_cN" executionOrderId="21">
                <position x="1130" y="80"/>
                <inputVariables>
                    <variable formalParameter="_DWORD" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_REAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="53"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_LREAL" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="_Status" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="110"/>
                            <connection refLocalId="52"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="130"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="70" height="20" width="140" negated="false">
                <position x="1310" y="160"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_DCA_SF</expression>
            </inVariable>
            <inVariable localId="71" height="20" width="140" negated="false">
                <position x="1310" y="120"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_M4_DCA</expression>
            </inVariable>
            <inVariable localId="72" height="20" width="140" negated="false">
                <position x="1310" y="100"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_M3_DCA</expression>
            </inVariable>
            <inVariable localId="75" height="20" width="140" negated="false">
                <position x="1310" y="140"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_I_DC_RD</expression>
            </inVariable>
            <block localId="76" width="155" height="160" typeName="Measurement_DC_SunS" instanceName="F_I_DC" executionOrderId="22">
                <position x="1470" y="80"/>
                <inputVariables>
                    <variable formalParameter="in_1" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="72"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="in_2" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="71"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                            <connection refLocalId="75"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                            <connection refLocalId="70"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="78" height="20" width="140" negated="false">
                <position x="1640" y="120"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_M4_DCV</expression>
            </inVariable>
            <inVariable localId="79" height="20" width="140" negated="false">
                <position x="1640" y="140"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>F_U_DC_RD</expression>
            </inVariable>
            <inVariable localId="83" height="20" width="140" negated="false">
                <position x="1640" y="160"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_DCV_SF</expression>
            </inVariable>
            <inVariable localId="84" height="20" width="140" negated="false">
                <position x="1640" y="100"/>
                <connectionPointOut>
                    <relPosition x="140" y="10"/>
                </connectionPointOut>
                <expression>Fronius_M3_DCV</expression>
            </inVariable>
            <block localId="87" width="155" height="160" typeName="Measurement_DC_SunS" instanceName="F_U_DC" executionOrderId="24">
                <position x="1800" y="80"/>
                <inputVariables>
                    <variable formalParameter="in_1" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="84"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="in_2" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="78"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="RetainData" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                            <connection refLocalId="79"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="scale" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="90"/>
                            <connection refLocalId="83"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="OUT" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="30"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="OUT_aux" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="50"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="70"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="HW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="90"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LW_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="110"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="LA_act" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="130"/>
                        </connectionPointOut>
                    </variable>
                    <variable formalParameter="Error" negated="false">
                        <connectionPointOut>
                            <relPosition x="155" y="150"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
        </FBD>
    </body>
</pou>
