(*Funkcia rob� z Real premennej string ale len s jedn�m desatin�m miestom*)
FUNCTION roundReal2 : STRING
VAR_INPUT
Val	:REAL;
Precision :INT;
END_VAR

VAR
auxString	:STRING;
AuxStart	:STRING;
X			:SINT;
Y			:SINT;
posComma	:INT;
Lenght		:INT;
END_VAR
auxString:=REAL_TO_STRING(Val);
posComma:=FIND(STR1 := auxString, STR2 := '.');
Lenght:=LEN(STR := auxString); 
IF Val < 1 AND Val > 0 THEN
		AuxStart:='0.';
	ELSE
		AuxStart:=LEFT(STR := auxString, SIZE := posComma+(Precision-1));
	END_IF
IF Lenght > posComma+1 THEN
	X:=STRING_TO_SINT(MID(STR := auxString,LEN := 1,POS := posComma+Precision));
	Y:=STRING_TO_SINT(MID(STR := auxString,LEN := 1,POS := posComma+Precision+1));
	IF Y >5 THEN
		X:= X+1;
	END_IF
	roundReal2:=CONCAT(STR1 := AuxStart, STR2 := SINT_TO_STRING(X));
ELSE
	roundReal2:=auxString;
END_IF	
;
END_FUNCTION