FUNCTION BOOL_TO_lowerSTRING : STRING(7)
VAR_INPUT
in				: BOOL;
END_VAR

VAR
str				: STRING;
END_VAR
IF in THEN
	str:= 'true';
ELSE
	str:= 'false';
END_IF
BOOL_TO_lowerSTRING:=str;
END_FUNCTION