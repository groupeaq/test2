(*
 * Oct 9, 2023
*)
FUNCTION_BLOCK Error_
VAR_INPUT
	Error				:BOOL;			(*triger for save*)
	ErrorID				:UDINT;			(*Error to save*)
END_VAR

VAR_OUTPUT
	aError				:ARRAY[1..3] OF UDINT;
	aTime				:ARRAY[1..3] OF DT;
END_VAR

VAR
	n					:SINT:=1;		
	auxTrig				:R_TRIG;
END_VAR

auxTrig(S1 := Error, Q0 => );
IF auxTrig.Q0 THEN
	IF n>3 THEN
		n:=1;
	END_IF
	aError[n]:=ErrorID;
	n:=n+1;
END_IF

END_FUNCTION_BLOCK
