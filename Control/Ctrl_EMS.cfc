<?xml version="1.0" encoding="UTF-8"?>
<pou xmlns="http://www.plcopen.org/xml/tc6_0201" name="Ctrl_EMS" pouType="functionBlock">
    <interface>
        <localVars/>
        <addData>
            <data name="www.bachmann.at/plc/plcopenxml" handleUnknown="implementation">
                <textDeclaration>
                    <content>
(*
 * Oct 14, 2023
*)
FUNCTION_BLOCK Ctrl_EMS
VAR_INPUT

END_VAR

VAR_OUTPUT

END_VAR

VAR

END_VAR

					</content>
                </textDeclaration>
            </data>
        </addData>
    </interface>
    <body>
        <FBD>
            <inVariable localId="1" height="20" width="49" negated="false">
                <position x="930" y="190"/>
                <connectionPointOut>
                    <relPosition x="49" y="10"/>
                </connectionPointOut>
                <expression>0</expression>
            </inVariable>
            <inVariable localId="2" height="20" width="58" negated="false">
                <position x="1890" y="190"/>
                <connectionPointOut>
                    <relPosition x="58" y="10"/>
                </connectionPointOut>
                <expression>-1</expression>
            </inVariable>
            <inVariable localId="3" height="20" width="49" negated="false">
                <position x="1220" y="210"/>
                <connectionPointOut>
                    <relPosition x="49" y="10"/>
                </connectionPointOut>
                <expression>0</expression>
            </inVariable>
            <inVariable localId="4" height="20" width="85" negated="false">
                <position x="1440" y="210"/>
                <connectionPointOut>
                    <relPosition x="85" y="10"/>
                </connectionPointOut>
                <expression>10000</expression>
            </inVariable>
            <inVariable localId="5" height="20" width="103" negated="false">
                <position x="690" y="230"/>
                <connectionPointOut>
                    <relPosition x="103" y="10"/>
                </connectionPointOut>
                <expression>10000.0</expression>
            </inVariable>
            <inVariable localId="6" height="20" width="94" negated="false">
                <position x="220" y="190"/>
                <connectionPointOut>
                    <relPosition x="94" y="10"/>
                </connectionPointOut>
                <expression>1000.0</expression>
            </inVariable>
            <inVariable localId="7" height="20" width="112" negated="false">
                <position x="-70" y="190"/>
                <connectionPointOut>
                    <relPosition x="112" y="10"/>
                </connectionPointOut>
                <expression>Setpoint_1</expression>
            </inVariable>
            <inVariable localId="8" height="20" width="139" negated="false">
                <position x="930" y="170"/>
                <connectionPointOut>
                    <relPosition x="139" y="10"/>
                </connectionPointOut>
                <expression>Fronius_WChaMax</expression>
            </inVariable>
            <inVariable localId="9" height="20" width="94" negated="false">
                <position x="1440" y="170"/>
                <connectionPointOut>
                    <relPosition x="94" y="10"/>
                </connectionPointOut>
                <expression>-10000</expression>
            </inVariable>
            <inVariable localId="10" height="20" width="139" negated="false">
                <position x="220" y="250"/>
                <connectionPointOut>
                    <relPosition x="139" y="10"/>
                </connectionPointOut>
                <expression>Fronius_WChaMax</expression>
            </inVariable>
            <comment localId="11" height="23" width="40">
                <position x="210" y="110"/>
                <content>
                    <pre>4</pre>
                </content>
            </comment>
            <comment localId="12" height="23" width="1670">
                <position x="550" y="90"/>
                <content>
                    <pre>Prepočet setpointu tak aby do InWRte a do OutWRte sa zapísal žiadaný výkon v percentách (WChaMax je maximálny výkon meniča[W], čítaný z meniča)</pre>
                </content>
            </comment>
            <comment localId="13" height="42" width="338">
                <position x="280" y="110"/>
                <content>
                    <pre>setpoint sa zadáva v kW, prevod na W&#xd;
nastavuje sa z HMI</pre>
                </content>
            </comment>
            <block localId="14" width="68" height="60" typeName="MUL" executionOrderId="0">
                <position x="360" y="150"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="15"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="6"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="68" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="16" width="149" height="40" typeName="WORD_TO_REAL" executionOrderId="1">
                <position x="380" y="230"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="10"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="149" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="17" width="68" height="60" typeName="DIV" executionOrderId="2">
                <position x="560" y="150"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="14"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="16">
                                <position x="560" y="200"/>
                                <position x="530" y="200"/>
                                <position x="530" y="260"/>
                                <position x="529" y="260"/>
                            </connection>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="68" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="18" width="68" height="60" typeName="MUL" executionOrderId="3">
                <position x="810" y="190"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="17">
                                <position x="810" y="220"/>
                                <position x="770" y="220"/>
                                <position x="770" y="180"/>
                                <position x="628" y="180"/>
                            </connection>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="5"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="68" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="19" width="140" height="40" typeName="REAL_TO_INT" executionOrderId="4">
                <position x="940" y="230"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="18">
                                <position x="940" y="260"/>
                                <position x="910" y="260"/>
                                <position x="910" y="220"/>
                                <position x="878" y="220"/>
                            </connection>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="140" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="20" width="59" height="60" typeName="GT" executionOrderId="5">
                <position x="1090" y="150"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="8"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="1"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="59" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="21" width="68" height="80" typeName="SEL" executionOrderId="6">
                <position x="1310" y="170"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="20">
                                <position x="1310" y="200"/>
                                <position x="1260" y="200"/>
                                <position x="1260" y="180"/>
                                <position x="1149" y="180"/>
                            </connection>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="3"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                            <connection refLocalId="19">
                                <position x="1310" y="240"/>
                                <position x="1170" y="240"/>
                                <position x="1170" y="260"/>
                                <position x="1080" y="260"/>
                            </connection>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="68" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <block localId="22" width="86" height="80" typeName="LIMIT" executionOrderId="7">
                <position x="1570" y="150"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="9"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="21"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                            <connection refLocalId="4"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="86" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <outVariable localId="23" height="20" width="139" executionOrderId="8" negated="false" storage="none">
                <position x="1710" y="210"/>
                <connectionPointIn>
                    <relPosition x="0" y="10"/>
                    <connection refLocalId="22">
                        <position x="1710" y="220"/>
                        <position x="1644" y="220"/>
                        <position x="1644" y="200"/>
                        <position x="1656" y="180"/>
                    </connection>
                </connectionPointIn>
                <expression>Fronius_OutWRte</expression>
            </outVariable>
            <block localId="24" width="68" height="60" typeName="MUL" executionOrderId="9">
                <position x="1990" y="150"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="22"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="2"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="68" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <outVariable localId="25" height="20" width="130" executionOrderId="10" negated="false" storage="none">
                <position x="2120" y="170"/>
                <connectionPointIn>
                    <relPosition x="0" y="10"/>
                    <connection refLocalId="24"/>
                </connectionPointIn>
                <expression>Fronius_InWRte</expression>
            </outVariable>
            <block localId="15" width="74" height="80" typeName="SEL" executionOrderId="11">
                <position x="140" y="150"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="26">
                                <position x="140" y="180"/>
                                <position x="120" y="180"/>
                                <position x="120" y="140"/>
                                <position x="104" y="140"/>
                            </connection>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="7"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="70"/>
                            <connection refLocalId="27"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="74" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="27" height="20" width="112" negated="false">
                <position x="-70" y="210"/>
                <connectionPointOut>
                    <relPosition x="112" y="10"/>
                </connectionPointOut>
                <expression>AutoSetpoint</expression>
            </inVariable>
            <inVariable localId="28" height="20" width="112" negated="false">
                <position x="-110" y="130"/>
                <connectionPointOut>
                    <relPosition x="112" y="10"/>
                </connectionPointOut>
                <expression>AutoMan</expression>
            </inVariable>
            <block localId="26" width="74" height="60" typeName="AND" executionOrderId="12">
                <position x="30" y="110"/>
                <inputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointIn>
                            <relPosition x="0" y="30"/>
                            <connection refLocalId="28"/>
                        </connectionPointIn>
                    </variable>
                    <variable formalParameter="" negated="true">
                        <connectionPointIn>
                            <relPosition x="0" y="50"/>
                            <connection refLocalId="29"/>
                        </connectionPointIn>
                    </variable>
                </inputVariables>
                <inOutVariables/>
                <outputVariables>
                    <variable formalParameter="" negated="false">
                        <connectionPointOut>
                            <relPosition x="74" y="30"/>
                        </connectionPointOut>
                    </variable>
                </outputVariables>
            </block>
            <inVariable localId="29" height="20" width="112" negated="false">
                <position x="-110" y="150"/>
                <connectionPointOut>
                    <relPosition x="112" y="10"/>
                </connectionPointOut>
                <expression>EMSenable</expression>
            </inVariable>
        </FBD>
    </body>
</pou>
