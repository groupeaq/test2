FUNCTION OPCUA : INT
VAR_INPUT
	min1	 	:REAL;
	max1		:REAL;
END_VAR

MPC_IN.SOC_min.value:=min1;
MPC_IN.SOC_max.value:=max1;


IF Prev_ctrl <> MPC_OUT.Setpoint.value[0] THEN
	MPC_IN.Prev_control.value:=Prev_ctrl;
END_IF
Prev_ctrl:=MPC_OUT.Setpoint.value[0];
END_FUNCTION