(*
 * Feb 28, 2024
*)
FUNCTION_BLOCK Test
VAR_INPUT

Msg_arrived		:STRING;
Topic_arrived	:STRING;
trig			:BOOL;

END_VAR

VAR_OUTPUT

END_VAR

VAR
	x				:INT;
	TimeArray		:ARRAY[0..20] OF DT;
	MsgArray		:ARRAY[0..20] OF STRING;
	dummy			:DWORD;
END_VAR
IF trig THEN
	TimeArray[x] := Tcs_RTC_GetDateAndTime(dummy);
	MsgArray[x] := Msg_arrived;
	x:=x+1;
END_IF
IF x>20 THEN
	x:=0;
END_IF
END_FUNCTION_BLOCK
