(*
 * Oct 8, 2023
*)
FUNCTION_BLOCK MQTT_HandleRequets
VAR_INPUT
	NewMessage				: BOOL;
	RequestTopic			: STRING(1023);
	RequestMessage			: STRING(1023);
END_VAR

VAR_OUTPUT
	ProcessDataReq			: BOOL;
	CPUinfoReq				: BOOL;
	ExtCPUinfoReq			: BOOL;
	EthStatReq				: BOOL;
	CPUinfoReq_C			: BOOL;
	ExtCPUinfoReq_C			: BOOL;
	EthStatReq_C			: BOOL;
END_VAR
	
VAR
	AuxTrue					: INT;
	NewMessage_Rtrig		: R_TRIG;
	auxLEN					: INT;
	auxStart				: INT;
	auxEnd					: INT;
END_VAR
CPUinfoReq := FALSE;
ExtCPUinfoReq := FALSE;
EthStatReq := FALSE;
ProcessDataReq:= FALSE;
NewMessage_Rtrig (S1 := NewMessage);
IF NewMessage_Rtrig.Q0 THEN

	IF FIND (STR1 := RequestTopic, STR2 := 'PLC/request')>0 THEN
		auxLEN:= LEN(RequestMessage);
		IF FIND(RequestMessage, '"ControlWord": ')>0 THEN
			auxStart := FIND(RequestMessage, '"ControlWord": ') + LEN('"ControlWord": ');
    		auxEnd := FIND(MID(RequestMessage, auxLEN - auxStart+1, auxStart), ',');
    		EMS_ControlWord := MID(RequestMessage, auxEnd -1, auxStart);
    	END_IF
    	IF FIND(RequestMessage, '"Setpoint": ')>0THEN
    		auxStart := FIND(RequestMessage, '"Setpoint": ') + LEN('"Setpoint": ');
    		auxEnd := FIND(MID(RequestMessage, auxLEN - auxStart+1, auxStart), '}');
    		EMS_Setpoint := STRING_TO_REAL (MID(RequestMessage, auxEnd - 1, auxStart));
    	END_IF
    	IF FIND (STR1 := RequestMessage, STR2 := '"EMS_enable": true')>0 THEN
    		EMSenable:= TRUE;
    	END_IF
    	   IF FIND (STR1 := RequestMessage, STR2 := '"EMS_enable": false')>0 THEN
    		EMSenable:= FALSE;
    	END_IF;
 
	END_IF
	
	AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.ProcessData');
	IF AuxTrue>0 THEN
		ProcessDataReq:= TRUE;
		RETURN;
	END_IF
	
	AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.CPUinfo');
	IF AuxTrue>0 THEN
		CPUinfoReq:= TRUE;
		AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.Continuous: true');
		IF AuxTrue>0 THEN
			CPUinfoReq_C:= TRUE;
		ELSE
			CPUinfoReq_C:= FALSE;
		END_IF
		RETURN;
	END_IF
	
	AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.ExtCPUinfo');
	IF AuxTrue>0 THEN
		ExtCPUinfoReq:= TRUE;
		AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.Continuous: true');
		IF AuxTrue>0 THEN
			ExtCPUinfoReq_C:= TRUE;
		ELSE
			ExtCPUinfoReq_C:= FALSE;
		END_IF
		RETURN;
	END_IF
	
	AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.EthStat');
	IF AuxTrue>0 THEN
		EthStatReq:= TRUE;
		AuxTrue:=FIND (STR1 := RequestMessage, STR2 := '.Continuous: true');
		IF AuxTrue>0 THEN
			EthStatReq_C:= TRUE; 
		ELSE
			EthStatReq_C:= FALSE;
		END_IF
	END_IF
END_IF	

END_FUNCTION_BLOCK