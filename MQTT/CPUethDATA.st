(*
 * Feb 5, 2024
*)
FUNCTION_BLOCK CPUethDATA
VAR_INPUT
CPUinfo_Trig			: BOOL;
ExtCPUinfo_Trig			: BOOL;
EthStat_Trig			: BOOL;
END_VAR

VAR_OUTPUT
	trig			: BOOL;
	EndofTopic		: STRING(50);
	arrayToSend		: ARRAY[0..5000] OF BYTE;
	MessageLEN		: UDINT;
END_VAR

VAR
	CPUinfo				: MSYS_CPUINFO; 				(* CPU data *)
	ExtCPUinfo			: MSYS_EXTCPUINFO; 				(* extended CPU data *)
	EthStat				: MSYS_ETHSTAT; 				(* ETH data *)
	DaT					: DT;
    
	Init_CPU			: BOOL;
	Init_ExtCPU			: BOOL;
	Init_Eth			: BOOL;
	
    AuxString			: STRING(200);
    AuxString2			: STRING(1023);
   
	CPUinfo_Name		: ARRAY[0..17] OF STRING(50);
	ExtCPUinfo_Name		: ARRAY[0..44] OF STRING(50);
	EthStat_Name		: ARRAY[0..16] OF STRING(50);		
	array_Value			: ARRAY[0..44] OF STRING(50);

   	i					: INT;
   	y					: INT;
   	x					: DINT;
   	auxMID				: STRING(7);
	
   	memCopy				: DINT;
	dummy				: DINT;	
	
	bigest_name			: INT;
	bigest_value		: INT;						
END_VAR

trig:=CPUinfo_Trig OR ExtCPUinfo_Trig OR EthStat_Trig;

CPUinfo:= MSys_GetCpuInfo(dummy);
ExtCPUinfo:=CPUinfo.pExtCpuInfo^;			
EthStat:=CPUinfo.pEthStatist^;
DaT := Tcs_RTC_GetDateAndTime(dummy);

IF CPUinfo_Trig THEN 
	IF NOT Init_CPU THEN
		CPUinfo_Name[0] :='{$n$t"Timestamp": ';
		CPUinfo_Name[1]:='$n$t"RetCode": ';
		CPUinfo_Name[2]:='$n$t"CpuSwitch": ';
		CPUinfo_Name[3]:='$n$t"ProcNum": ';
		CPUinfo_Name[4]:='$n$t"CpuClock": ';
		CPUinfo_Name[5]:='$n$t"TickRate": ';
		CPUinfo_Name[6]:='$n$t"TimeSlice": ';
		CPUinfo_Name[7]:='$n$t"TotalTicks": ';
		CPUinfo_Name[8]:='$n$t"AuxClkRate": ';
		CPUinfo_Name[9]:='$n$t"RestartCount": ';
		CPUinfo_Name[10]:='$n$t"DebugMode": ';
		CPUinfo_Name[11]:='$n$t"SizeDRam": ';
		CPUinfo_Name[12]:='$n$t"SizeNVRam": ';
		CPUinfo_Name[13]:='$n$t"ProcType": ';
		CPUinfo_Name[14]:= '$n$t"EnableMMP": ';												
		CPUinfo_Name[15]:= '$n$t"HyperThreading": ';
		CPUinfo_Name[16]:='$n$t"NbOfCores": ';
		CPUinfo_Name[17]:='$n$t"ReservedCores": ';
		Init_CPU:=TRUE;
	END_IF
	array_Value[0] :=CONCAT(INSERT ('""', DELETE(DT_TO_STRING (DaT),3,1),1),',');
	array_Value[1]:=CONCAT(DINT_TO_STRING (CPUinfo.RetCode),',');
	array_Value[2]:=CONCAT(DWORD_TO_STRING (CPUinfo.CpuSwitch),',');
	array_Value[3]:=CONCAT(UDINT_TO_STRING (CPUinfo.ProcNum),',');
	array_Value[4]:=CONCAT(UDINT_TO_STRING (CPUinfo.CpuClock),',');
	array_Value[5]:=CONCAT(UDINT_TO_STRING (CPUinfo.TickRate),',');
	array_Value[6]:=CONCAT(UDINT_TO_STRING (CPUinfo.TimeSlice),',');
	array_Value[7]:=CONCAT(UDINT_TO_STRING (CPUinfo.TotalTicks),',');
	array_Value[8]:=CONCAT(UDINT_TO_STRING (CPUinfo.AuxClkRate),',');
	array_Value[9]:=CONCAT(UDINT_TO_STRING (CPUinfo.RestartCount),',');
	array_Value[10]:=CONCAT(DWORD_TO_STRING (CPUinfo.DebugMode),',');
	array_Value[11]:=CONCAT(UDINT_TO_STRING (CPUinfo.SizeDRam),',');
	array_Value[12]:=CONCAT(UDINT_TO_STRING (CPUinfo.SizeNVRam),',');
	array_Value[13]:=CONCAT(UDINT_TO_STRING (CPUinfo.ProcType),',');
	array_Value[14]:= CONCAT(BOOL_TO_lowerSTRING(in := CPUinfo.EnableMMP),',');												
	array_Value[15]:= CONCAT(BOOL_TO_lowerSTRING(in := CPUinfo.HyperThreading),',');
	array_Value[16]:=CONCAT(BYTE_TO_STRING (CPUinfo.NbOfCores),',');
	array_Value[17]:=CONCAT(BYTE_TO_STRING (CPUinfo.ReservedCores),'$n}');
	x:=0;
	FOR y:=0 TO 17 DO
		FOR i:=1 TO LEN(CPUinfo_Name[y]) DO
			auxMID:= MID(CPUinfo_Name[y], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
		END_FOR
		FOR i:=1 TO LEN(array_Value[y]) DO
			auxMID:= MID(array_Value[y], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
		END_FOR
	END_FOR
	MessageLEN:=x;
	EndofTopic:='/CPUinfo';
	RETURN;
END_IF
(* ExtCPUinfo*)
IF ExtCPUinfo_Trig THEN
	IF NOT Init_ExtCPU THEN
		ExtCPUinfo_Name[0] :='{$n$t"Timestamp": ';
		ExtCPUinfo_Name[1]:='$n$t"CurrTemp": ';
		ExtCPUinfo_Name[2]:='$n$t"MinTemp": ';
		ExtCPUinfo_Name[3]:='$n$t"MaxTemp": ';
		ExtCPUinfo_Name[4]:='$n$t"CpuUsage100Low": ';
		ExtCPUinfo_Name[5]:='$n$t"CpuUsage100High": ';
		ExtCPUinfo_Name[6]:='$n$t"CpuUsage1000Low": ';
		ExtCPUinfo_Name[7]:='$n$t"CpuUsage1000High": ';
		ExtCPUinfo_Name[8]:='$n$t"CpuUsageTotalLow": ';
		ExtCPUinfo_Name[9]:='$n$t"CpuUsageTotalHigh": ';
		ExtCPUinfo_Name[10]:='$n$t"TotalTimeLow": ';
		ExtCPUinfo_Name[11]:='$n$t"TotalTimeHigh": ';
		(* spare array *)
		ExtCPUinfo_Name[12]:='$n$t"UnitsTime": ';
		ExtCPUinfo_Name[13]:='$n$t"LastIdleTime": ';
		ExtCPUinfo_Name[14]:='$n$t"MaxPccErases": ';
		ExtCPUinfo_Name[15]:='$n$t"MaxFlashErases": ';
		ExtCPUinfo_Name[16]:='$n$t"StrayInterrupts": ';
		ExtCPUinfo_Name[17]:='$n$t"SyncHigh": ';
		ExtCPUinfo_Name[18]:='$n$t"SyncLow": ';
		ExtCPUinfo_Name[19]:='$n$t"SyncCount": ';
		ExtCPUinfo_Name[20]:='$n$t"AuxClkCount": ';
		ExtCPUinfo_Name[21]:='$n$t"PccPresent": ';
		ExtCPUinfo_Name[22]:='$n$t"PccPluggedIn": ';
		ExtCPUinfo_Name[23]:='$n$t"PccWritable": ';
		ExtCPUinfo_Name[24]:='$n$t"PccCheckError": ';
		ExtCPUinfo_Name[25]:='$n$t"FlashPresent": ';
		ExtCPUinfo_Name[26]:='$n$t"FlashWritable": ';
		ExtCPUinfo_Name[27]:='$n$t"FlashCheckError": ';
		ExtCPUinfo_Name[28]:='$n$t"NVRamSupplError": ';
		ExtCPUinfo_Name[29]:='$n$t"NVRamCheckError": ';
		ExtCPUinfo_Name[30]:='$n$t"StackCheckError": ';
		ExtCPUinfo_Name[31]:='$n$t"DRamCheckError": ';
		ExtCPUinfo_Name[32]:='$n$t"RTCError": ';
		ExtCPUinfo_Name[33]:='$n$t"SoftBootError": ';
		ExtCPUinfo_Name[34]:='$n$t"HardBootError": ';
		ExtCPUinfo_Name[35]:='$n$t"LongNameEnable": ';
		ExtCPUinfo_Name[36]:='$n$t"TickSource": ';
		ExtCPUinfo_Name[37]:='$n$t"SntpTimestamp": "'; 
		ExtCPUinfo_Name[38]:='$n$t"RebootReason": ';
		ExtCPUinfo_Name[39]:='$n$t"RebootInf": "';
		ExtCPUinfo_Name[40]:='$n$t"SntpTimeRecv": {$n$t$t$t"DateAndTime": "';
		ExtCPUinfo_Name[41]:='$n$t$t$t"NanoSeconds": ';   			
		ExtCPUinfo_Name[42]:='$n$t"SntpTimeCorr": {$n$t$t$t"DateAndTime": "';
		ExtCPUinfo_Name[43]:='$n$t$t$t"NanoSeconds": ';			
		ExtCPUinfo_Name[44]:='$n$t"SntpMaxCorrection": ';
		Init_ExtCPU:=TRUE;
	END_IF
	array_Value[0] :=CONCAT(INSERT ('""', DELETE(DT_TO_STRING (DaT),3,1),1),',');
	array_Value[1]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.CurrTemp),',');
	array_Value[2]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.MinTemp),',');
	array_Value[3]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.MaxTemp),',');
	array_Value[4]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.CpuUsage100Low),',');
	array_Value[5]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.CpuUsage100High),',');
	array_Value[6]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.CpuUsage1000Low),',');
	array_Value[7]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.CpuUsage1000High),',');
	array_Value[8]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.CpuUsageTotalLow),',');
	array_Value[9]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.CpuUsageTotalHigh),',');
	array_Value[10]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.TotalTimeLow),',');
	array_Value[11]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.TotalTimeHigh),',');
	(* spare array *)
	array_Value[12]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.UnitsTime),',');
	array_Value[13]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.LastIdleTime),',');
	array_Value[14]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.MaxPccErases),',');
	array_Value[15]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.MaxFlashErases),',');
	array_Value[16]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.StrayInterrupts),',');
	array_Value[17]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.StrayInterrupts),',');
	array_Value[18]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.SyncLow),',');
	array_Value[19]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.pSyncCount^),',');
	array_Value[20]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.pAuxClkCount^),',');
	array_Value[21]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.PccPresent),',');
	array_Value[22]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.PccPluggedIn),',');
	array_Value[23]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.PccWritable),',');
	array_Value[24]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.PccCheckError),',');
	array_Value[25]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.FlashPresent),',');
	array_Value[26]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.FlashWritable),',');
	array_Value[27]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.FlashCheckError),',');
	array_Value[28]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.NVRamSupplError),',');
	array_Value[29]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.NVRamCheckError),',');
	array_Value[30]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.StackCheckError),',');
	array_Value[31]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.DRamCheckError),',');
	array_Value[32]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.RTCError),',');
	array_Value[33]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.SoftBootError),',');
	array_Value[34]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.HardBootError),',');
	array_Value[35]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.LongNameEnable),',');
	array_Value[36]:=CONCAT(BYTE_TO_STRING (ExtCPUinfo.TickSource),',');
	array_Value[37]:=CONCAT(DATE_TO_STRING (ExtCPUinfo.SntpTimestamp),'",');
	array_Value[38]:=CONCAT(UDINT_TO_STRING (ExtCPUinfo.RebootReason),',');
	array_Value[39]:=CONCAT(ExtCPUinfo.RebootInf,'",');
	array_Value[40]:=CONCAT(DT_TO_STRING(ExtCPUinfo.SntpTimeRecv.DateAndTime),'",');
	array_Value[41]:=CONCAT(UDINT_TO_STRING(ExtCPUinfo.SntpTimeRecv.NanoSeconds),'$n$t},');
	array_Value[42]:=CONCAT(DT_TO_STRING(ExtCPUinfo.SntpTimeCorr.DateAndTime),'",');
	array_Value[43]:=CONCAT(UDINT_TO_STRING(ExtCPUinfo.SntpTimeCorr.NanoSeconds),'$n$t},');
	array_Value[44]:=CONCAT(DWORD_TO_STRING (ExtCPUinfo.SntpMaxCorrection),'$n}');
	x:=0;
	FOR y:=0 TO 44 DO
		FOR i:=1 TO LEN(ExtCPUinfo_Name[y]) DO
			auxMID:= MID(ExtCPUinfo_Name[y], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
			IF LEN(ExtCPUinfo_Name[y]) > bigest_name THEN bigest_name:=LEN(ExtCPUinfo_Name[y]); END_IF
		END_FOR
		FOR i:=1 TO LEN(array_Value[y]) DO
			auxMID:= MID(array_Value[y], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
			IF LEN(array_Value[y]) > bigest_value THEN bigest_value:=LEN(array_Value[y]); END_IF
		END_FOR
		MessageLEN:=x;
		EndofTopic:='/ExtCPUinfo';
	END_FOR
END_IF
(*EthStat*)
IF EthStat_Trig THEN
	IF NOT Init_Eth THEN
		EthStat_Name[0] :='{$n$t"Timestamp": ';
		EthStat_Name[1] :='$n$t"InOverloadCount": ';
		EthStat_Name[2]:='$n$t"OutOverloadCount": ';
		EthStat_Name[3]:='$n$t"BcOverloadCount": ';
		EthStat_Name[4]:='$n$t"InBytes100msPeak": ';
		EthStat_Name[5]:='$n$t"BcBytes100msPeak": ';
		EthStat_Name[6]:='$n$t"OutBytes100msPeak": ';
		EthStat_Name[7]:='$n$t"CurrIndex100s": ';
		EthStat_Name[8]:='$n$t"InPacksTotal": ';
		EthStat_Name[9]:='$n$t"InBytesTotalLow": ';
		EthStat_Name[10]:='$n$t"InBytesTotalHigh": ';
		EthStat_Name[11]:='$n$t"BcPacksTotal": ';
		EthStat_Name[12]:='$n$t"BcBytesTotalLow": ';
		EthStat_Name[13]:='$n$t"BcBytesTotalHigh": ';
		EthStat_Name[14]:='$n$t"OutPacksTotal": ';
		EthStat_Name[15]:='$n$t"OutBytesTotalLow": ';
		EthStat_Name[16]:='$n$t"OutBytesTotalHigh": ';
		Init_Eth:=TRUE;
	END_IF
	array_Value[0] :=CONCAT(INSERT ('""', DELETE(DT_TO_STRING (DaT),3,1),1),',');
	array_Value[1]:=CONCAT(UDINT_TO_STRING (EthStat.InOverloadCount),',');
	array_Value[2]:=CONCAT(UDINT_TO_STRING (EthStat.OutOverloadCount),',');
	array_Value[3]:=CONCAT(UDINT_TO_STRING (EthStat.BcOverloadCount),',');
	array_Value[4]:=CONCAT(UDINT_TO_STRING (EthStat.InBytes100msPeak),',');
	array_Value[5]:=CONCAT(UDINT_TO_STRING (EthStat.BcBytes100msPeak),',');
	array_Value[6]:=CONCAT( UDINT_TO_STRING (EthStat.OutBytes100msPeak),',');
	array_Value[7]:=CONCAT(UDINT_TO_STRING (EthStat.CurrIndex100s),',');
	array_Value[8]:=CONCAT(UDINT_TO_STRING (EthStat.InPacksTotal),',');
	array_Value[9]:=CONCAT(DWORD_TO_STRING (EthStat.InBytesTotalLow),',');
	array_Value[10]:=CONCAT(DWORD_TO_STRING (EthStat.InBytesTotalHigh),',');
	array_Value[11]:=CONCAT(UDINT_TO_STRING (EthStat.BcPacksTotal),',');
	array_Value[12]:=CONCAT(DWORD_TO_STRING (EthStat.BcBytesTotalLow),',');
	array_Value[13]:=CONCAT(DWORD_TO_STRING (EthStat.BcBytesTotalHigh),',');
	array_Value[14]:=CONCAT(UDINT_TO_STRING (EthStat.OutPacksTotal),',');
	array_Value[15]:=CONCAT(DWORD_TO_STRING (EthStat.OutBytesTotalLow),',');
	array_Value[16]:=CONCAT(DWORD_TO_STRING (EthStat.OutBytesTotalHigh),',');
	x:=0;
	FOR y:=0 TO 16 DO
		FOR i:=1 TO LEN(EthStat_Name[y]) DO
			auxMID:= MID(EthStat_Name[y], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
			IF LEN(EthStat_Name[y]) > bigest_name THEN bigest_name:=LEN(EthStat_Name[y]); END_IF
		END_FOR
		FOR i:=1 TO LEN(array_Value[y]) DO
			auxMID:= MID(array_Value[y], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
			IF LEN(array_Value[y]) > bigest_value THEN bigest_value:=LEN(array_Value[y]); END_IF
		END_FOR
	END_FOR
	(*StartTicks10ms*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.StartTicks10ms[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.StartTicks10ms[i]));
		END_IF
	END_FOR
	SETLARGESTRINGS(TRUE);
	AuxString:=INSERT(
		STR1 :='$n$t"StartTicks10ms": [],',
		STR2 := AuxString,
		POS := 21
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*InBytes10ms*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.InBytes10ms[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.InBytes10ms[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"InBytes10ms": [],',
		STR2 := AuxString,
		POS := 18
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*BcBytes10ms*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.BcBytes10ms[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.BcBytes10ms[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"BcBytes10ms": [],',
		STR2 := AuxString,
		POS := 18
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*OutBytes10ms*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.OutBytes10ms[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.OutBytes10ms[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"OutBytes10ms": [],',
		STR2 := AuxString,
		POS := 19
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*InPacks100s*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.InPacks100s[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.InPacks100s[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"InPacks100s": [],',
		STR2 := AuxString,
		POS := 18
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*InBytes100s*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.InBytes100s[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.InBytes100s[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"InBytes100s": [],',
		STR2 := AuxString,
		POS := 18
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*BcPacks100s*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.BcPacks100s[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.BcPacks100s[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"BcPacks100s": [],',
		STR2 := AuxString,
		POS := 18
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*BcBytes100s*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.BcBytes100s[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.BcBytes100s[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"BcBytes100s": [],',
		STR2 := AuxString,
		POS := 18
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*OutPacks100s*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.OutPacks100s[i]);
		ELSE
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.OutPacks100s[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"OutPacks100s": [],',
		STR2 := AuxString,
		POS := 19
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR
	(*OutBytes100s*)
	FOR i:=1 TO 10 DO
		IF i=1 THEN
			AuxString:=UDINT_TO_STRING (EthStat.OutBytes100s[i]);
		ELSE 
			AuxString:=CONCAT(AuxString ,', ');
			AuxString:=CONCAT(AuxString ,UDINT_TO_STRING (EthStat.OutBytes100s[i]));
		END_IF
	END_FOR
	AuxString:=INSERT(
		STR1 :='$n$t"OutBytes100s": []$n}',
		STR2 := AuxString,
		POS := 19
	);
	FOR i:=1 TO LEN(AuxString) DO
		auxMID:= MID(AuxString, 1, i);
		memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
		x:=x+1;
	END_FOR	
	MessageLEN:=x;
	EndofTopic:='/EthStat';
	RETURN;
END_IF
END_FUNCTION_BLOCK
