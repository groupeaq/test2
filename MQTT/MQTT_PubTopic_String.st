FUNCTION_BLOCK MQTT_PubTopic_String
VAR_INPUT
	Execute				: BOOL;						(*Rise edge start function*)
	Continuous			: BOOL;						(*False=send topics only once and close session*)
	Period				: TIME;								
	EndOfTopic			: STRING;					(*Topic to send*)
	pMessage			: POINTER TO STRING;					(*Message to send*)
	PubMessageLen		: UDINT;
	QoS					: UDINT;
END_VAR
VAR_OUTPUT
	ErrorPub			: BOOL;	
	Done				: BOOL;
	Busy				: BOOL;
	Errors				:ARRAY[1..3] OF UDINT;
	Times				:ARRAY[1..3] OF DT;	
END_VAR
VAR
	Publish				: BOOL;
	pOpen				: POINTER TO MQTTP_OPEN;
	ID					: UDINT;
	Option				: MQTTP_OPEN;
	PubFunc				: MQTTP_PublishMessage;
	QueryFunc			: MQTTP_QueryMessage;

	OpenSessionTrig		: R_TRIG;
	CloseSessionTrig	: R_TRIG;
	n					:SINT:=1;		
	ErrTrig				:R_TRIG;
	
	auxTimer1	:TON; 
    auxTimer2	:TOF; 
    pulse		:BOOL;
    Rtrig		:R_TRIG;
    Ftrig		:F_TRIG; 
    topic		:STRING;
END_VAR
IF Continuous THEN
	auxTimer1(IN:=NOT auxTimer2.Q, PT:= Period);
	Rtrig(S1 := auxTimer1.Q);
	auxTimer2(IN:=Rtrig.Q0, PT:= T#100ms); 
	pulse := auxTimer2.Q;
END_IF
pOpen:=ADR(Option);
Option.pSessionId:=ADR(ID);

OpenSessionTrig(S1 := Execute, Q0 =>);
IF OpenSessionTrig.Q0 THEN
	MQTTP_OpenSession(pOpen);
	Publish:= TRUE;
END_IF;
topic:=CONCAT(StartOfTopic,EndOfTopic);
PubFunc(
	Execute := (Publish AND NOT Continuous) OR (Continuous AND pulse),
	SessionId := ID,
	pTopicName := ADR(topic),
	TopicLen := LEN(topic),
	pMessage := pMessage,
	MessageLen := PubMessageLen,
	Quality := QoS,
	pOptions := ,
	Done => Done,
	Busy => Busy,
	Error => ,
	ErrorID => 
);
ErrTrig(S1 := PubFunc.Error, Q0 => );
IF ErrTrig.Q0 THEN
	IF n>3 THEN
		n:=1;
	END_IF
	Errors[n]:=PubFunc.ErrorID;
	n:=n+1;
END_IF
IF NOT Continuous THEN
	Publish:= FALSE;
	CloseSessionTrig(S1 := (PubFunc.Done OR PubFunc.Error) AND NOT PubFunc.Busy, Q0 =>);
END_IF

IF CloseSessionTrig.Q0 THEN  			(*Max Session=127. All pending resources related to this session will be closed.*)
	MQTTP_CloseSession(ID);	
END_IF;

END_FUNCTION_BLOCK