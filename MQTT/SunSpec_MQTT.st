(*
 * Jan 11, 2024
*)
FUNCTION_BLOCK SunSpec_MQTT
VAR_INPUT
	trig				:BOOL;
	Name_1				:STRING(50);
	Name_2				:STRING(50);
	Name_3				:STRING(50);
	Name_4				:STRING(50);
	Name_5				:STRING(50);
	Name_6				:STRING(50);
	Name_7				:STRING(50);
	Name_8				:STRING(50);
	Name_9				:STRING(50);
	Name_10				:STRING(50);
	Name_11				:STRING(50);
	Name_12				:STRING(50);	
	Name_13				:STRING(50);
	Name_14				:STRING(50);
	Value_1				:REAL;
	Value_2				:REAL;
	Value_3				:REAL;
	Value_4				:REAL;
	Value_5				:REAL;
	Value_6				:REAL;
	Value_7				:REAL;
	Value_8				:REAL;
	Value_9				:REAL;
	Value_10			:REAL;
	Value_11			:REAL;
	Value_12			:REAL;
	Value_13			:REAL;
	Value_14			:REAL;
	Hys_1				:REAL;
	Hys_2				:REAL;
	Hys_3				:REAL;
	Hys_4				:REAL;
	Hys_5				:REAL;
	Hys_6				:REAL;
	Hys_7				:REAL;
	Hys_8				:REAL;
	Hys_9				:REAL;
	Hys_10				:REAL;
	Hys_11				:REAL;
	Hys_12				:REAL;
	Hys_13				:REAL;
	Hys_14				:REAL;
	Units_1				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_2				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_3				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_4				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_5				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_6				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_7				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_8				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_9				:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_10			:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_11			:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_12			:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_13			:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	Units_14			:STRING(8);(*1=bool, 2 =real, 3=int, 4=string*)
	NumberAct			:INT;
	Precision			:INT;
END_VAR

VAR_OUTPUT

END_VAR

VAR
	n					:INT;
	x					:INT;
	y					:INT;
	i					:INT;
	auxMID				:STRING(7);
	memCopy				:DINT;
	Pos					:INT;
	ToSend				:BOOL;
	
	OLD_1				:REAL;
	OLD_2				:REAL;	
	OLD_3				:REAL;
	OLD_4				:REAL;
	OLD_5				:REAL;
	OLD_6				:REAL;
	OLD_7				:REAL;
	OLD_8				:REAL;
	OLD_9				:REAL;
	OLD_10				:REAL;
	OLD_11				:REAL;
	OLD_12				:REAL;
	OLD_13				:REAL;
	OLD_14				:REAL;
	a_Names				:ARRAY [0..15] OF STRING(50);
	a_Values			:ARRAY [0..15] OF STRING(50);
	arrayToSend			:ARRAY [0..5000] OF BYTE;
	MsgLEN				:INT;
	
	MQTT_publish		:MQTT_PubTopic_Array;
END_VAR
x:=0;
a_Names[1]:=Name_1;
a_Names[2]:=Name_2;
a_Names[3]:=Name_3;
a_Names[4]:=Name_4;
a_Names[5]:=Name_5;
a_Names[6]:=Name_6;
a_Names[7]:=Name_7;
a_Names[8]:=Name_8;
a_Names[9]:=Name_9;
a_Names[10]:=Name_10;
a_Names[11]:=Name_11;
a_Names[12]:=Name_12;
a_Names[13]:=Name_13;
a_Names[14]:=Name_14;
a_Names[15]:='';

a_Names[0]:=ActDaT_to_String();
a_Values[0]:=''; 
Pos:=0;
CreateSendData;
IF trig THEN 
	ToSend:=TRUE;
	a_Values[15]:='$n$t"ManReguest": true,';
	Pos:=15;
	CreateSendData;
END_IF


IF ((Value_1 <= (OLD_1-Hys_1)) OR (Value_1 >= (OLD_1+Hys_1))) OR trig THEN 
	ToSend:=TRUE;
	OLD_1:=Value_1;
	IF Units_1='bool' THEN
		a_Values[1]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_1)),',');
	ELSIF Units_1='status_i' THEN
		a_Values[1]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_1)),',');
	ELSE
		a_Values[1]:=CONCAT(roundReal(Val := Value_1,Precision:=1),',');
	END_IF
	Pos:=1;
	CreateSendData;	
END_IF


IF ((Value_2 <= (OLD_2-Hys_2)) OR (Value_2 >= (OLD_2+Hys_2))) OR trig THEN 
	ToSend:=TRUE;
	OLD_2:=Value_2;
	IF Units_2='bool' THEN
		a_Values[2]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_2)),',');
	ELSIF Units_2='status_i' THEN
		a_Values[2]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_2)),',');
	ELSE
		a_Values[2]:=CONCAT(roundReal(Val := Value_2,Precision:=1),',');
	END_IF
	Pos:=2;
	CreateSendData;
END_IF

IF ((Value_3 <= (OLD_3-Hys_3)) OR (Value_3 >= (OLD_3+Hys_3))) OR trig THEN 
	ToSend:=TRUE;
	OLD_3:=Value_3;
	IF Units_3='bool' THEN
		a_Values[3]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_3)),',');
	ELSIF Units_3='status_i' THEN
		a_Values[3]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_3)),',');
	ELSE
		a_Values[3]:=CONCAT(roundReal(Val := Value_3,Precision:=1),',');
	END_IF
	Pos:=3;
	CreateSendData;
END_IF

IF ((ABS(Value_4) <= (OLD_4-Hys_4)) OR (ABS(Value_4) >= (OLD_4+Hys_4))) OR trig THEN (*opravit ked bude viac �asu!!!!!!!!!*)
	ToSend:=TRUE;
	OLD_4:=ABS(Value_4);
	IF Units_4='bool' THEN
		a_Values[4]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_4)),',');
	ELSIF Units_4='status_i' THEN
		a_Values[4]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_4)),',');
	ELSE
		a_Values[4]:=CONCAT(roundReal(Val := Value_4,Precision:=1),',');
	END_IF
	Pos:=4;
	CreateSendData;
END_IF

IF ((Value_5 <= (OLD_5-Hys_5)) OR (Value_5 >= (OLD_5+Hys_5))) OR trig THEN
	ToSend:=TRUE;
	OLD_5:=Value_5;
	IF Units_5='bool' THEN
		a_Values[5]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_5)),',');
	ELSIF Units_5='status_i' THEN
		a_Values[5]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_5)),',');
	ELSE
		a_Values[5]:=CONCAT(roundReal(Val := Value_5,Precision:=1),',');
	END_IF
	Pos:=5;
	CreateSendData;
END_IF

IF ((Value_6 <= (OLD_6-Hys_6)) OR (Value_6 >= (OLD_6+Hys_6))) OR trig THEN
	ToSend:=TRUE;
	OLD_6:=Value_6;
	IF Units_6='bool' THEN
		a_Values[6]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_6)),',');
	ELSIF Units_6='status_i' THEN
		a_Values[6]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_6)),',');
	ELSE
		a_Values[6]:=CONCAT(roundReal(Val := Value_6,Precision:=1),',');
	END_IF
	Pos:=6;
	CreateSendData;
END_IF

IF ((Value_7 <= (OLD_7-Hys_7)) OR (Value_7 >= (OLD_7+Hys_7))) OR trig THEN
	ToSend:=TRUE;
	OLD_7:=Value_7;
	IF Units_7='bool' THEN
		a_Values[7]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_7)),',');
	ELSIF Units_7='status_i' THEN
		a_Values[7]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_7)),',');
	ELSE
		a_Values[7]:=CONCAT(roundReal(Val := Value_7,Precision:=1),',');
	END_IF
	Pos:=7;
	CreateSendData;
END_IF

IF ((Value_8 <= (OLD_8-Hys_8)) OR (Value_8 >= (OLD_8+Hys_8))) OR trig THEN
	ToSend:=TRUE;
	OLD_8:=Value_8;
	IF Units_8='bool' THEN
		a_Values[8]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_8)),',');
	ELSIF Units_8='status_i' THEN
		a_Values[8]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_8)),',');
	ELSE 
		a_Values[8]:=CONCAT(roundReal(Val := Value_8,Precision:=1),',');
	END_IF
	Pos:=8;
	CreateSendData;
END_IF

IF ((Value_9 <= (OLD_9-Hys_9)) OR (Value_9 >= (OLD_9+Hys_9))) OR trig THEN 
	ToSend:=TRUE;
	OLD_9:=Value_9;
	IF Units_9='bool' THEN
		a_Values[9]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_9)),',');
	ELSIF Units_9='status_i' THEN
		a_Values[9]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_9)),',');
	ELSE
		a_Values[9]:=CONCAT(roundReal(Val := Value_9,Precision:=1),',');
	END_IF
	Pos:=9;
	CreateSendData;	
END_IF

IF ((Value_10 <= (OLD_10-Hys_10)) OR (Value_10 >= (OLD_10+Hys_10))) OR trig THEN 
	ToSend:=TRUE;
	OLD_10:=Value_10;
	IF Units_10='bool' THEN
		a_Values[10]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_10)),',');
	ELSIF Units_10='status_i' THEN
		a_Values[10]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_10)),',');
	ELSE
		a_Values[10]:=CONCAT(roundReal(Val := Value_10,Precision:=1),',');
	END_IF
	Pos:=10;
	CreateSendData;	
END_IF

IF ((Value_11 <= (OLD_11-Hys_11)) OR (Value_11 >= (OLD_11+Hys_11))) OR trig THEN 
	ToSend:=TRUE;
	OLD_11:=Value_11;
	IF Units_11='bool' THEN
		a_Values[11]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_11)),',');
	ELSIF Units_11='status_i' THEN
		a_Values[11]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_11)),',');
	ELSE
		a_Values[11]:=CONCAT(roundReal(Val := Value_11,Precision:=1),',');
	END_IF
	Pos:=11;
	CreateSendData;	
END_IF

IF ((Value_12 <= (OLD_12-Hys_12)) OR (Value_12 >= (OLD_12+Hys_12))) OR trig THEN 
	ToSend:=TRUE;
	OLD_12:=Value_12;
	IF Units_12='bool' THEN
		a_Values[12]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_12)),',');
	ELSIF Units_12='status_i' THEN
		a_Values[12]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_12)),',');
	ELSE
		a_Values[12]:=CONCAT(roundReal(Val := Value_12,Precision:=1),',');
	END_IF
	Pos:=12;
	CreateSendData;	
END_IF

IF ((Value_13 <= (OLD_13-Hys_13)) OR (Value_13 >= (OLD_13+Hys_13))) OR trig THEN 
	ToSend:=TRUE;
	OLD_13:=Value_13;
	IF Units_13='bool' THEN
		a_Values[13]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_13)),',');
	ELSIF Units_13='status_i' THEN
		a_Values[13]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_13)),',');
	ELSE
		a_Values[13]:=CONCAT(roundReal(Val := Value_13,Precision:=3),',');
	END_IF
	Pos:=13;
	CreateSendData;	
END_IF

IF ((Value_14 <= (OLD_14-Hys_14)) OR (Value_14 >= (OLD_14+Hys_14))) OR trig THEN 
	ToSend:=TRUE;
	OLD_14:=Value_14;
	IF Units_14='bool' THEN
		a_Values[14]:=CONCAT(BOOL_TO_lowerSTRING(in := REAL_TO_BOOL(Value_14)),',');
	ELSIF Units_14='status_i' THEN
		a_Values[14]:=CONCAT(INT_TO_STRING(REAL_TO_INT(Value_14)),',');
	ELSE
		a_Values[14]:=CONCAT(roundReal(Val := Value_14,Precision:=1),',');
	END_IF
	Pos:=14;
	CreateSendData;	
END_IF

x:=x-1;
FOR i:=1 TO LEN('$n}') DO
	auxMID:= MID('$n}', 1, i);
	memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
	x:=x+1;
END_FOR
IF MQTT_publish.Busy THEN
	ToSend:=FALSE;
END_IF
MsgLEN:=x;
MQTT_publish(
	Execute := ToSend,
	Continuous := FALSE,
	Period := T#0s,
	EndOfTopic := '/ProcessValues',
	pMessage := ADR(arrayToSend),
	PubMessageLen := MsgLEN,
	QoS := QoS_c,
	ErrorPub => ,
	Done => ,
	Busy => ,
	Errors => ,
	Times => 
);
ToSend :=FALSE;

IF  EMSenable THEN
	IF STRING_TO_WORD (EMS_ControlWord)>3 OR STRING_TO_WORD (EMS_ControlWord)<=0 THEN
		Fronius_StorCtl_Mod:=3;
		Setpoint_1:=EMS_Setpoint;
	ELSE
		Fronius_StorCtl_Mod:= STRING_TO_WORD (EMS_ControlWord);
		Setpoint_1:=EMS_Setpoint;
	END_IF
ELSE 
	IF (HMI_ControlWord>3 OR HMI_ControlWord<=0) THEN
		Fronius_StorCtl_Mod:=3;
		Setpoint_1:=HMI_setpoint;
	ELSE
		Fronius_StorCtl_Mod:=HMI_ControlWord;
		Setpoint_1:=HMI_setpoint;
END_IF
END_IF
END_FUNCTION_BLOCK
ACTIONS
	ACTION CreateSendData
		FOR i:=1 TO LEN(a_Names[Pos]) DO
			auxMID:= MID(a_Names[Pos], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
		END_FOR
		FOR i:=1 TO LEN(a_Values[Pos]) DO
			auxMID:= MID(a_Values[Pos], 1, i);
			memCopy:=Mem_Copy(ADR(arrayToSend[x]),ADR(auxMID),1);
			x:=x+1;
		END_FOR
	END_ACTION
END_ACTIONS
