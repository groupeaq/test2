(*
 * Oct 7, 2023
 *)
FUNCTION_BLOCK MQTT_SubTopic
VAR_INPUT
	Execute			: BOOL; (*Rise edge start function*)
	EndOfTopic		: STRING(50); (*Topic to send*)
	QoS				: UDINT;
END_VAR

VAR_OUTPUT
	STOP_sub			: BOOL;
	Done_sub			: BOOL;
    Busy_sub 			: BOOL; 
    Error_sub 			: BOOL; 
    ErrorID_sub 		: DINT; 

	MsgArrived		: BOOL;
	ArrivedTopic	: STRING(255);
	arrivedMsg		: STRING(255);
	ConnectionLost	: BOOL;
	ConnectionStatus: DINT;
	Error_QueryID	: DINT;
END_VAR

VAR
	Topic			: STRING;
	SubMessage		: ARRAY[0..9] OF STRING(255);

	pSubTopic		: POINTER TO STRING;
	pSubMessage		: POINTER TO STRING;
	pArrivedTopic	: POINTER TO STRING;
	pOpen			: POINTER TO MQTTP_OPEN;

	
	SubTopicLen		: UDINT;
	ID				: UDINT;
	Option			: MQTTP_OPEN;
	SubFunc			: MQTTP_SubscribeTopic;
	QueryFunc		: MQTTP_QueryMessage;
	SubErrors		: Error_;

	OpenSessionTrig	: R_TRIG;
	CloseSessionTrig: R_TRIG;
	MsgArrivedTrig	: R_TRIG;
	
END_VAR
Topic:=CONCAT(StartOfTopic,EndOfTopic);
arrivedMsg:=SubMessage[0];
OpenSessionTrig(S1 := Execute, Q0 => );

IF OpenSessionTrig.Q0 THEN
	pOpen := ADR(Option);
	Option.pSessionId := ADR(ID);
	MQTTP_OpenSession(pOpen);
END_IF

QueryFunc(
	SessionId := ID,
	ErrorID => Error_QueryID,
	MessageArrived => MsgArrived,
	ConnectionLost => ConnectionLost,
	ConnectionStatus => ConnectionStatus
);
MsgArrivedTrig(S1 := MsgArrived);
IF MsgArrivedTrig.Q0 THEN
	SubMessage[9]:=SubMessage[8];
	SubMessage[8]:=SubMessage[7];
	SubMessage[7]:=SubMessage[6];
	SubMessage[6]:=SubMessage[5];
	SubMessage[5]:=SubMessage[4];
	SubMessage[4]:=SubMessage[3];
	SubMessage[3]:=SubMessage[2];
	SubMessage[2]:=SubMessage[1];
	SubMessage[1]:=SubMessage[0];
END_IF

Topic:='EAQv1/DnV/labak/PLC/request';
pSubTopic := ADR(Topic);
SubTopicLen := LEN(Topic);
pSubMessage := ADR(SubMessage[0]);
pArrivedTopic := ADR(ArrivedTopic);

IF QoS<0 THEN QoS:=0; END_IF
IF QoS>2 THEN QoS:=2; END_IF

SubFunc(
	Execute := Execute,
	SessionId := ID,
	pTopicName := pSubTopic,
	TopicLen := SubTopicLen,
	pTopicArrived := pArrivedTopic,
	TopicLenArrived := 255,
	pMessage := pSubMessage,
	MessageLen := 1023,
	Quality := QoS,
	Done => Done_sub, 
    Busy => Busy_sub,
    Error => Error_sub, 
    ErrorID => ErrorID_sub 
	);
SubErrors(
	Error := SubFunc.Error,
	ErrorID := SubFunc.ErrorID,
	aError => ,
	aTime => 
	);
	
CloseSessionTrig(S1 := NOT Execute AND NOT SubFunc.Busy AND (SubFunc.Done OR SubFunc.Error));
IF CloseSessionTrig.Q0 THEN 				(*Max Session=127. All pending resources related to this session will be closed.*)
	MQTTP_CloseSession(ID);
	STOP_sub := TRUE;
END_IF

END_FUNCTION_BLOCK