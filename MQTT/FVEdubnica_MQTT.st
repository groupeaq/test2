(*
 * Jan 11, 2024
*)
FUNCTION_BLOCK FVEdubnica_MQTT
VAR_INPUT
	trig				:BOOL;
END_VAR

VAR_OUTPUT

END_VAR

VAR
	ToSend				:BOOL;
	dummy				:INT;
	DaT					:DT;
	
	Name				:ARRAY[0..5] OF STRING(40);
	Value				:ARRAY[0..5] OF STRING(40);
	SendData			:ARRAY[0..1000] OF BYTE;
	MessageLEN			:INT;
	MQTT_publish		:MQTT_PubTopic_Array;
	i					:INT;
	x					:INT;
	y					:INT;
	auxMID				:STRING(7);
	memCopy				:DINT;
	
	auxTimer1	:TON; 
    auxTimer2	:TOF;  
    Rtrig		:R_TRIG;
    Ftrig		:F_TRIG;
END_VAR

RH2_DP_P_impT1:=RH2_DemandPeriod_P_impT1/1000.0;
FVE_DP_P_expT1:=FVE_DemandPeriod_P_expT1/1000.0;
POC_DP_P_impT1:=POC_DemandPeriod_P_impT1/1000.0;
POC_DP_P_expT1:=POC_DemandPeriod_P_expT1/1000.0;

DaT := Tcs_RTC_GetDateAndTime(dummy);
auxTimer1(IN:=NOT auxTimer2.Q, PT:= T#899s);
Rtrig(S1 := auxTimer1.Q);
auxTimer2(IN:=Rtrig.Q0, PT:= T#1s); 
ToSend := auxTimer2.Q;

IF ToSend OR trig THEN
Name[0]:='{$n$t"Timestamp": ';
Name[1]:='$n$t"RH2_DemandPeriod_P_impT1": ';
Name[2]:='$n$t"FVE_DemandPeriod_P_expT1": ';
Name[3]:='$n$t"POC_DemandPeriod_P_impT1": ';
Name[4]:='$n$t"POC_DemandPeriod_P_expT1": ';

Value[0]:=CONCAT(INSERT ('""', DELETE(DT_TO_STRING (DaT),3,1),1),',');
Value[1]:=CONCAT(roundReal(Val := RH2_DP_P_impT1, Precision := 2),',');
Value[2]:=CONCAT(roundReal(Val := FVE_DP_P_expT1, Precision := 2),',');
Value[3]:=CONCAT(roundReal(Val := POC_DP_P_impT1, Precision := 2),',');
Value[4]:=CONCAT(roundReal(Val := POC_DP_P_expT1, Precision := 2),'$n}');
x:=0;
	FOR y:=0 TO 4 DO
		FOR i:=1 TO LEN(Name[y]) DO
			auxMID:= MID(Name[y], 1, i);
			memCopy:=Mem_Copy(ADR(SendData[x]),ADR(auxMID),1);
			x:=x+1;
		END_FOR
		FOR i:=1 TO LEN(Value[y]) DO
			auxMID:= MID(Value[y], 1, i);
			memCopy:=Mem_Copy(ADR(SendData[x]),ADR(auxMID),1);
			x:=x+1;
		END_FOR
	END_FOR
MessageLEN:=x;
END_IF
MQTT_publish(
	Execute := ToSend OR trig,
	Continuous := FALSE,
	Period := T#0s,
	EndOfTopic := '/FVE',
	pMessage := ADR(SendData),
	PubMessageLen := MessageLEN,
	QoS := QoS_c,
	ErrorPub => ,
	Done => ,
	Busy => ,
	Errors => ,
	Times => 
);
END_FUNCTION_BLOCK
