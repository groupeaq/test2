function registerMousePan(target) {
    var clicked = false, clickY, clickX;
    var left, top;
    document.addEventListener("mousemove", function (e) {
        if (clicked) {
            updateScrollPos(e);
        }
    }, true);
    document.addEventListener("mousedown", function (e) {
        clicked = true;
        clickY = e.pageY;
        clickX = e.pageX;
        left = translateX;
        top = translateY;
    }, true);
    document.addEventListener("mouseup", function (e) {
        clicked = false;
    }, true);

    var updateScrollPos = function (e) {
        translateX = left - (clickX - e.pageX);
        translateY = top - (clickY - e.pageY);
        target.setAttribute("transform", "translate(" + translateX + ", " + translateY + ") scale(" + scale + ")");
    };

    setCursorForPanZoom();
}

function registerMouseZoomToRect() {
    actionNavDoc.addEventListener("mousedown", down, false);
    actionNavDoc.addEventListener("mouseup", up, false);
    actionNavDoc.addEventListener("mousemove", drawRect, false);
}

var down = function (e) {
    if (click == 0) {
        startX = e.pageX;
        startY = e.pageY;
        x2 = startX;
        y2 = startY;
    }
    if (click > 0) {
        x2 = e.clientX;
        y2 = e.clientY;
    }
    click++;
    toggleRect(e);
};

var up = function (e) {
    checkIfClick();
    if (clickIsValid) {
        toggleRect(e);
    } else {
        click++;
        toggleRect(e);
    }
    clickIsValid = true;
};

function drawRect(e) {
    x2 = e.clientX;
    y2 = e.clientY;
    reCalc();
}

function removeMouseZoomToRect() {
    actionNavDoc.removeEventListener("mousedown", down, false);
    actionNavDoc.removeEventListener("mouseup", up, false);
    actionNavDoc.removeEventListener("mousemove", drawRect, false);
}