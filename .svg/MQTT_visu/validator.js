function validate() {
    var input = this.value;
    var datatype = "";
    //looking for STRING because: default string = STRING / custom string with range e.g: STRING(10)
    var str = this.getAttribute("data-type");
    if (str.indexOf("STRING") > -1) {
        datatype = "STRING";
    } else {
        datatype = str;
    }
    switch (datatype) {
        case "BYTE":
        case "USINT":
            numberValidator(0, 255, input);
            break;
        case "SINT":
            numberValidator(-128, 127, input);
            break;
        case "WORD":
        case "UINT":
            numberValidator(0, 65535, input);
            break;
        case "INT":
            numberValidator(-32768, 32767, input);
            break;
        case "DWORD":
        case "UDINT":
            numberValidator(0, 4294967295, input);
            break;
        case "DINT":
            numberValidator(-2147483648, 2147483648, input);
            break;
        case "REAL":
            doubleValidator(parseFloat('-3.4028235e+38f', 10), parseFloat('3.4028235e+38f', 10), input);
            break;
        case "LREAL":
            doubleValidator(parseFloat('-1.7976931348623157e+308'), parseFloat('1.7976931348623157e+308'), input);
            break;
        case "STRING":
            stringValidator(input, str);
            break;
        case "BOOL":
            boolValidator(input);
            break;
    }
}

function numberValidator(min, max, input) {
    if (input < min || input > max || !input.match("^[-+]?[0-9]*?$")) {
        setFalse();
    } else {
        setTrue();
    }
}

function doubleValidator(min, max, input) {
    if (parseFloat(input) < min || parseFloat(input) > max || !input.match("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+[fF]?)?$")) {
        setFalse();
    } else {
        setTrue();
    }
}

function stringValidator(input, datatype) {
    //default max string length 80 chars, else look for the max length in datatype
    var max = "";
    if (datatype == "STRING") {
        max = 80;
    } else {
        max = datatype.match(/\d+/g).map(Number);
    }

    if (input.length > max) {
        setFalse();
    } else {
        setTrue();
    }
}

function boolValidator(input) {
    if (input.toUpperCase() === "TRUE" || input.toUpperCase() === "FALSE" || input == 0 || input == 1) {
        setTrue();
    } else {
        setFalse();
    }
}

function setFalse() {
    var inputText = getInputText();
    inputText.style.background = "red";
    var writeButton = getWriteButton();
    writeButton.disabled = true;
    canSubmit = false;
}

function setTrue() {
    var inputText = getInputText();
    inputText.style.background = "white";
    var writeButton = getWriteButton();
    writeButton.disabled = false;
    canSubmit = true;
}

function getInputText() {
    return document.getElementById('input-text');
}

function getWriteButton() {
    return document.getElementById('write-btn');
}