function setHeight() {
	var parentDoc = parent.document;

	var indexHeader = parentDoc.getElementById("header");
	var indexHeaderHeight = indexHeader.getBoundingClientRect().height;

	var bodyHeight = parentDoc.body.clientHeight
	// calculate height for content div in index
	var indexContentHeight = bodyHeight - indexHeaderHeight;

	var footer = document.getElementById("footer");
	var footerHeight = footer.getBoundingClientRect().height;
	// calculate action-nav content height
	var actionNavContentHeight = indexContentHeight - footerHeight;

	var indexContent = parent.document.getElementById("index-content");
	indexContent.style.height = indexContentHeight + "px";

	var actionNavContent = document.getElementById("action-nav-content");
	actionNavContent.style.height = actionNavContentHeight + "px";
}

function getIframe() {
	return document.getElementById('svg-frame');
}

function changeContent(path) {
	var iframe = getIframe();
	iframe.src = path;
}

function toggleActions(element) {
	var actionMenu = document.getElementById("action-menu");
	if (element.checked) {
		actionMenu.style.display = "block";
	} else {
		actionMenu.style.display = "none";
	}
}

// dialogs

function registerEditDialogCloseListeners() {
	document.addEventListener("keydown", editDialogKey);
	document.addEventListener("click", editDialogClick);
	document.addEventListener("touchend", editDialogClick);
}

function removeEditDialogCloseListeners() {
	document.removeEventListener("keydown", editDialogKey);
	document.removeEventListener("click", editDialogClick);
	document.removeEventListener("touchend", editDialogClick);
}

var editDialogKey = function (event) {
	// esc key
	if (event.which == 27) {
		closeEditDialog();
	}
};

var editDialogClick = function (event) {
	if (document.getElementById('edit-dialog').contains(event.target)) {
		//do nothing when click inside edit-dialog
	} else {
		closeEditDialog();
	}
};

function registerInstanceDialogCloseListeners() {
	document.addEventListener("keydown", instanceDialogKey);
	document.addEventListener("click", instanceDialogClick);
	document.addEventListener("touchend", instanceDialogClick);
}

function removeInstanceDialogCloseListeners() {
	document.removeEventListener("keydown", instanceDialogKey);
	document.removeEventListener("click", instanceDialogClick);
	document.removeEventListener("touchend", instanceDialogClick);
}

var instanceDialogKey = function (event) {
	// esc key
	if (event.which == 27) {
		closeInstanceDialog();
	}
};

var instanceDialogClick = function (event) {
	if (document.getElementById('instance-dialog').contains(event.target)) {
		//do nothing when click inside edit-dialog
	} else {
		closeInstanceDialog();
	}
};

//edit-dialog edit values
var varPath = "";
var canSubmit = true;

function openEditDialog(value, path, datatype) {
	var editDialog = document.getElementById('edit-dialog-level');
	editDialog.style.display = "block";
	editDialog.addEventListener("keyup", editDialogKeyListener);

	var input = getInput();
	input.value = value;
	input.setAttribute("data-type", datatype);
	input.addEventListener("keyup", validate);

	document.getElementById("edit-dialog-title").innerText = path;
	varPath = path;
	selectInput();
	registerEditDialogCloseListeners();
}

function editDialogKeyListener(event) {
	// F7
	if (event.which == 118) {
		readOldValue();
	}
	// F8
	if (event.which == 119 && canSubmit) {
		writeValue();
	}
	// enter
	if (event.which == 13 && canSubmit) {
		writeValue();
		closeEditDialog();
	}
}

function readOldValue() {
	var input = getInput();
	var svgFrame = parent.getSvgFrame();
	input.value = svgFrame.contentWindow.getValue(varPath);
	resetValidation();
	selectInput();
}

function writeValue() {
	var input = getInput();
	var newValue = input.value;
	webMI.data.write("/".concat(varPath), newValue);
	selectInput();
}

function closeEditDialog() {
	var editDialog = document.getElementById('edit-dialog-level');
	editDialog.removeEventListener("keyup", editDialogKeyListener);

	var input = getInput();
	input.removeEventListener("keyup", validate);
	input.removeAttribute("data-type");
	resetValidation();
	editDialog.style.display = 'none';
	removeEditDialogCloseListeners();
}

function selectInput() {
	var input = getInput();
	input.focus();
	input.select();
}

function resetValidation() {
	var input = getInput();
	input.style.background = "white";
	document.getElementById('write-btn').disabled = false;
	canSubmit = true;
}

function getInput() {
	return document.getElementById('input-text');
}

/* instance dialog */

var storageKey;
function openInstanceDialog() {
	var dialog = document.getElementById("instance-dialog-level");
	dialog.style.display = "block";

	// focus input when open dialog
	var input = document.getElementById("instance-input");
	input.focus();

	// set storageKey which is used to store used instances
	storageKey = parent.document.getElementById("content-frame").src;

	disableSubmitButton();
	loadLastInstances();

	registerInstanceDialogCloseListeners();
	registerKeyNavigationListener();
	registerEnterSubmitListener();
}

function closeInstanceDialog() {
	var dialog = document.getElementById("instance-dialog-level");
	dialog.style.display = "none";

	resetInstance();
	resetFilter();

	removeLastInstances();
	removeInstanceDialogCloseListeners();
	removeKeyNavigationListener();
	removeEnterSubmitListener();
}

function updateVisibleInstances() {
	var input, filter, ul, li, label, i, txtValue;
	input = document.getElementById("instance-input");
	filter = input.value.toUpperCase();
	ul = document.getElementById("instance-ul");
	li = ul.getElementsByTagName("li");

	for (i = 0; i < li.length; i++) {
		label = li[i].getElementsByTagName("label")[0];
		txtValue = label.innerText;

		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			li[i].style.display = "";
		} else {
			li[i].style.display = "none";
		}
	}
}

function resetFilter() {
	var input = document.getElementById("instance-input");
	input.value = "";
	updateVisibleInstances();
}

var selectedInstance;
function selectInstance(li) {
	selectedInstance = li;
	enableSubmitButton();
}

function enableSubmitButton() {
	var btn = document.getElementById("instance-dialog-select");
	btn.disabled = false;
}

function disableSubmitButton() {
	var btn = document.getElementById("instance-dialog-select");
	btn.disabled = true;
}

function submitInstance() {
	var path = selectedInstance.getAttribute("data-path");
	saveInstance(path);
	changeInstance(path);
	resetInstance();
	closeInstanceDialog();
}

function changeInstance(instance) {
	window.instance = instance;
	var iframe = getIframe();
	// need to refresh so the svg script will be retriggered
	// now the script can access the instance to get the right values
	iframe.src = iframe.src;
	changeActiveInstanceLabel(instance);
}

function saveInstance(path) {
	// create entry if not already created
	if (sessionStorage.getItem(storageKey) === null) {
		sessionStorage.setItem(storageKey, path);
	} else {
		var list = sessionStorage.getItem(storageKey).split(",");
		// rearrange order if path is included
		if (list.indexOf(path) > -1) {
			var index = list.indexOf(path);
			list.splice(index, 1);
			// remove first entry and include new one, limit to 5 entries
		} else if (list.length == 5) {
			list.splice(0, 1);
			// include entry
		}
		list.push(path);
		sessionStorage.setItem(storageKey, list.join(","));
	}
}

function changeActiveInstanceLabel(path) {
	label = parent.document.getElementById("active-instance");
	label.innerText = instance;
}

function resetInstance() {
	if (selectedInstance != null) {
		selectedInstance = undefined;
	}
	disableSubmitButton();
}

var list, lastUsedInstancesList, first, input;
function registerKeyNavigationListener() {
	list = document.getElementById("instance-ul");

	if (hasUsedInstances) {
		lastUsedInstancesList = document.getElementById("instance-last-ul");
		first = lastUsedInstancesList.childNodes[1];
	} else {
		first = list.firstElementChild;
	}
	input = document.getElementById("instance-input");
	document.addEventListener("keydown", keyNavigationListener);
}

function removeKeyNavigationListener() {
	document.removeEventListener("keydown", keyNavigationListener);
}

var keyNavigationListener = function (event) {
	switch (event.which) {
		// up
		case 38:
			if (getPreviousVisibleElement(document.activeElement) == input) {
				resetInstance();
				input.focus();
			} else {
				var li = getPreviousVisibleElement(document.activeElement);
				li.focus();
				selectInstance(li);
			}
			break;
		// down
		case 40:
			var li = getNextVisibleElement(document.activeElement);
			if (li != null) {
				li.focus();
				selectInstance(li);
			}
			break;
	}
};

function getNextVisibleElement(element) {
	// get next element
	var nextElement;
	if (element == input) {
		if (hasUsedInstances) {
			// get the first element form last instaces list if this list exists
			var listLast = document.getElementById("instance-last-ul");
			nextElement = listLast.childNodes[1];
		} else {
			// get the first element from the initial list if last instaces list doesn't exist
			nextElement = input.nextElementSibling.firstElementChild;
		}
	} else {
		nextElement = element.nextElementSibling;
	}
	if (nextElement == null && hasUsedInstances && document.activeElement.parentNode.getAttribute("id") == "instance-last-ul") {
		// if last used instaces exist and the focus is on the last element from the list
		// get the first element from the initial list
		nextElement = getNextVisibleElement(list.childNodes[1]);
	}
	if (nextElement == null) {
		// return if there is no next element
		return;
	}
	// check if next element is visible (filter!) if not look for the next visible element
	if (window.getComputedStyle(nextElement).display == "block") {
		return nextElement;
	} else {
		return getNextVisibleElement(nextElement);
	}
}

function getPreviousVisibleElement(element) {
	// get previous element
	var previousElement = element.previousElementSibling;
	// in this case the top element is reached, there is no previous list item, return input
	if (previousElement == null || previousElement.tagName == "LABEL") {
		return input;
	}
	// if last used instances exists and the focus is on the top element in the normal list
	// get the last element in the last instaces list
	if (hasUsedInstances && previousElement.tagName == "UL") {
		previousElement = lastUsedInstancesList.childNodes[lastUsedInstancesList.childNodes.length - 1];
	}
	// check if previous element is visible (filter!) if not look for previous element
	if (window.getComputedStyle(previousElement).display == "block") {
		return previousElement;
	} else {
		return getPreviousVisibleElement(previousElement);
	}
}

function registerEnterSubmitListener() {
	document.addEventListener("keyup", enterSubmit);
}

function removeEnterSubmitListener() {
	document.removeEventListener("keyup", enterSubmit);
}

var enterSubmit = function (event) {
	// enter
	if (event.which == 13) {
		var submitBtn = document.getElementById("instance-dialog-select");
		submitBtn.click();
	}
};

var hasUsedInstances;
function loadLastInstances() {
	// look if sessionStorage contains any instance for acutal storageKey
	if (sessionStorage.getItem(storageKey) !== null) {
		// create new list for last used instances
		var lastInstancesList = htmlToElement('<ul id="instance-last-ul"></ul>');
		var last = htmlToElement('<label class="instance-last-label">Last Used Instances</label>');
		lastInstancesList.appendChild(last);

		// create and include html element in list created above
		var instancesList = sessionStorage.getItem(storageKey).split(",");
		instancesList.forEach(function (item) {
			includeElement(item, lastInstancesList);
			hasUsedInstances = true;
		});

		// insert the created list
		var list = document.getElementById("instance-ul");
		list.insertBefore(lastInstancesList, list.childNodes[0]);
	}
}

function removeLastInstances() {
	if (hasUsedInstances) {
		var list = document.getElementById("instance-ul");
		list.removeChild(document.getElementById("instance-last-ul"));
	}
}

function includeElement(path, lastInstancesList) {
	var id = storageKey + "/" + path;
	var li = htmlToElement('<li class="instance-li" id="' + id + '" data-path="' + path + '" onclick="selectInstance(this)" tabindex="-1"></li>');
	var label = htmlToElement('<label for="' + id + '" class="instance-label">' + path + '</label>');
	li.appendChild(label);

	// index 1 because label is at index 0, insert after label
	lastInstancesList.insertBefore(li, lastInstancesList.childNodes[1]);
}

function htmlToElement(html) {
	var template = document.createElement('template');
	html = html.trim(); // Never return a text node of whitespace as the result
	template.innerHTML = html;
	// template.content = undefined in ie
	return template.firstElementChild || template.content.firstElementChild;
}

var instanceName;
function openPouFrom(pouName) {
	parent.openPou(pouName, getInstanceName());
}

function openPouActionFromSrc(pouName, srcPouName, actionName) {
	getIframe().contentWindow.removeHoverInfo();
	var instanceName = getInstanceName();
	if (pouName == srcPouName) {
		openLocalAction(actionName);
		if (instanceName) {
			changeInstance(instanceName);
		}
	} else {
		parent.openPouAction(pouName, actionName, instanceName);
	}
}

function openLocalAction(actionName) {
	changeContent(actionName + ".svg");
	document.getElementById(actionName + "-label").click();
}

var fbUndefined = "__undefined__";

function getInstanceName() {
	if (instanceName == fbUndefined){
		instanceName = undefined;
		alert("The reference to the function block instance could not be resolved.\nThis function block may not be currently executed.\nThe default instance is opened instead.");
	}
	return instanceName;
}

function setInstanceName(name, fbpou) {
	if (fbpou){
		if (window.instance)
			instanceName = window.instance + "." +name;
		else
			instanceName = fbUndefined;
	}else{
		instanceName = name;
	}
}