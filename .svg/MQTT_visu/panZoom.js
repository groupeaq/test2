var firstX, firstY;
var firstWidth, firstHeight;
var translateX, translateY;
var scale = 1;
const MAX_SCALE = 4;
const MIN_SCALE = 0.25;
var indexHtml;

function initializeXYWidthHeight(target) {
	firstX = target.transform.baseVal.getItem(0).matrix.e; // transalteX
	firstY = target.transform.baseVal.getItem(0).matrix.f; // transalteY
	translateX = firstX;
	translateY = firstY;
	// offset to left and right + diagram width
	firstWidth = (target.getBBox().x + firstX) * 2 + target.getBBox().width;
	// offset to top and bottom + diagram height
	firstHeight = (target.getBBox().y + firstY) * 2 + target.getBBox().height;
}

function initializeIndexHtml(index) {
	indexHtml = index;
}

function registerPan(target) {
	registerTouchPan(target);
	registerMousePan(target);
}

function registerZoom(target) {
	registerMouseWheelZoomListener(target);
	registerKeyZoomListener(target);
}

function registerKeyZoomListener(target) {
	document.addEventListener("keydown", function (event) {
		if (event.ctrlKey == true && (event.which == '187' || event.which == '189' || event.which == '171' || event.which == '173' || event.which == '48' || event.which == '109' || event.which == '107' || event.which == '96' || event.which == '61')) {
			event.preventDefault();
			// + / numpad + / firefox + / firefox ENG +
			if (event.which == '187' || event.which == '107' || event.which == '171' || event.which == '61') {
				zoomIn(target);
				// - / numpad - / firefox -
			} else if (event.which == '189' || event.which == '109' || event.which == '173') {
				zoomOut(target);
				// 48 -> 0 / 96 -> numpad 0
			} else {
				zoomReset(target);
			}
		}
	});
}

function registerMouseWheelZoomListener(target) {
	document.addEventListener('mousewheel', function fn(event) {
		if (event.ctrlKey) {
			event.preventDefault();
			executeMouseWheelZoom(event, target);
		}
	}, { passive: false });
	document.addEventListener('DOMMouseScroll', function fn(event) {
		if (event.ctrlKey) {
			event.preventDefault();
			executeMouseWheelZoom(event, target);
		}
	}, { passive: false });
}

function executeMouseWheelZoom(event, target) {
	var newScale = getPreviousScaleValue();
	if (event.wheelDelta > 0 || event.detail < 0) {
		newScale = getNextScaleValue();
	}
	zoomToPoint(event.pageX, event.pageY, newScale, target);
}

function setCursorForPanZoom() {
	var svg = document.getElementsByTagName("svg")[0];
	document.addEventListener("keydown", function (e) {
		if (e.key == "Alt") {
			svg.setAttribute("cursor", "all-scroll");
			e.preventDefault();
		}
	}, true);
	document.addEventListener("keyup", function (e) {
		if (e.key == "Alt") {
			svg.setAttribute("cursor", "default");
			e.preventDefault();
		}
	}, true);
}

function zoomToPoint(x, y, newScale, target) {
	var oldScale = scale;
	setScale(newScale);

	var xBeforeScale = x;
	var yBeforeScale = y;

	var scaledX = (xBeforeScale - translateX) / oldScale * scale + translateX;
	var scaledY = (yBeforeScale - translateY) / oldScale * scale + translateY;

	translateX = translateX + xBeforeScale - scaledX;
	translateY = translateY + yBeforeScale - scaledY;

	target.setAttribute("transform", "translate(" + translateX + ", " + translateY + ") scale(" + scale + ")");
}

function zoomIn(target) {
	var centerCoords = getCenterCoords();
	zoomToPoint(centerCoords.x, centerCoords.y, getNextScaleValue(), target);
}

function zoomOut(target) {
	var centerCoords = getCenterCoords();
	zoomToPoint(centerCoords.x, centerCoords.y, getPreviousScaleValue(), target);
}

function zoomReset(target) {
	var centerCoords = getCenterCoords();
	zoomToPoint(centerCoords.x, centerCoords.y, 1, target);
}

function getCenterCoords() {
	var frameElement = getFrameElement();
	var centerCoords = {
		x: frameElement.width / 2,
		y: frameElement.height / 2
	};
	return centerCoords;
}

function fitToScreen(target) {
	// place at initial position
	translateX = firstX * scale;
	translateY = firstY * scale;

	// calculate scale
	var frameElement = getFrameElement();
	var scaleWidth = frameElement.width / firstWidth;
	var scaleHeight = frameElement.height / firstHeight;
	var newScale = Math.min(scaleWidth, scaleHeight);

	zoomToPoint(0, 0, newScale, target);
}

var selection,
	actionNavDoc,
	actionFrame,
	svgFrame,
	target,
	x1 = 0,
	y1 = 0,
	x2 = 0,
	y2 = 0;

function startRect(aNavDoc, aFrame, t) {
	target = t;
	actionFrame = aFrame;
	actionNavDoc = aNavDoc;
	selection = actionNavDoc.getElementById("selection");
	svgFrame = actionNavDoc.getElementById("svg-frame");
	addSelectionListeners(actionNavDoc);
}

function addSelectionListeners() {
	/*
	down/up => using mousedown/up event to look for click and drag when selecting area to zoom
	compares coordinates from mousedown/up event and calculate if it was a click or drag
	click => start with first click, end with second click
	drag => start with mousedown end with mouseup
	*/
	registerTouchZoomToRect();
	registerMouseZoomToRect();
}

var click = 0;
var clickIsValid = true;
var startX, startY;

function checkIfClick() {
	if (x2 < startX + 2 && x2 > startX - 2 && y2 < startY + 2 && y2 > startY - 2) {
		clickIsValid = true;
	} else {
		clickIsValid = false;
	}
}

function toggleRect(e) {
	if (click == 2) {
		reCalc();
		zoomToSelection();
		indexHtml.zoomRect();
	} else {
		selection.style.display = "block";
		x1 = startX;
		y1 = startY;
		reCalc();
	}
}

function reCalc() {
	var x3 = Math.min(x1, x2);
	var x4 = Math.max(x1, x2);
	var y3 = Math.min(y1, y2);
	var y4 = Math.max(y1, y2);
	selection.style.left = x3 + 'px';
	selection.style.top = y3 + 'px';
	selection.style.width = x4 - x3 + 'px';
	selection.style.height = y4 - y3 + 'px';
}

function zoomToSelection() {
	// selected area values
	var left = parseInt(selection.style.left);
	var top = parseInt(selection.style.top);
	var width = parseInt(selection.style.width);
	var height = parseInt(selection.style.height);

	zoomToRect(left, top, width, height);
}

function zoomToRect(left, top, width, height) {
	// get old scale
	var oldScale = scale;

	// get center coordinates from svg display area
	var centerCoords = getCenterCoords();

	// calculate center from selection area
	var selectionCX = left + width / 2;
	var selectionCY = top + height / 2;

	// move selection center to display center
	translateX += centerCoords.x - selectionCX;
	translateY += centerCoords.y - selectionCY;

	// calculate scale
	var computedStyle = actionFrame.contentWindow.getComputedStyle(svgFrame);
	var parentWidth = parseInt(computedStyle.width);
	var parentHeight = parseInt(computedStyle.height);

	var scaleWidth = parentWidth / width * oldScale;
	var scaleHeight = parentHeight / height * oldScale;
	var newScale = Math.min(scaleWidth, scaleHeight);

	// zoom to display center
	zoomToPoint(centerCoords.x, centerCoords.y, newScale, target);
}

function stopRect(actionNavDoc) {
	click = 0;
	selection.style.display = "none";
	removeSelectionListeners(actionNavDoc);
}

function removeSelectionListeners() {
	removeTouchZoomToRect();
	removeMouseZoomToRect();
}

function setScale(newScale) {
	scale = Math.min(MAX_SCALE, Math.max(MIN_SCALE, newScale));
	indexHtml.updateZoomValue(scale);
}

function getNextScaleValue() {
	if (scale >= 0.25 && scale < 1.5) {
		return Math.ceil(4 * scale + 0.001) / 4;
	} else if (scale >= 1.5 && scale < 2) {
		return Math.ceil(2 * scale + 0.001) / 2;
	} else if (scale >= 2 && scale < 4) {
		return Math.ceil(scale + 0.001);
	}
	return 4;
}

function getPreviousScaleValue() {
	if (scale >= 0.5 && scale <= 1.5) {
		return Math.max(0.25, Math.floor(4 * scale - 0.001) / 4);
	} else if (scale > 1.5 && scale <= 2) {
		return Math.floor(2 * scale - 0.001) / 2;
	} else if (scale > 2 && scale <= 4) {
		return Math.floor(scale - 0.001);
	}
	return 0.25;
}