function registerTouchPan(target) {
    var clicked = false, clickY, clickX;
    var left, top;
    document.addEventListener("touchmove", function (e) {
        if (clicked) {
            updateScrollPos(e);
        }
    }, true);
    document.addEventListener("touchstart", function (e) {
        clicked = true;
        clickY = e.touches[0].clientY;
        clickX = e.touches[0].clientX;
        left = translateX;
        top = translateY;
    }, true);
    document.addEventListener("touchend", function (e) {
        clicked = false;
    }, true);

    var updateScrollPos = function (e) {
        translateX = left - (clickX - e.touches[0].clientX);
        translateY = top - (clickY - e.touches[0].clientY);
        target.setAttribute("transform", "translate(" + translateX + ", " + translateY + ") scale(" + scale + ")");
    };

    setCursorForPanZoom();
}

function registerTouchZoomToRect() {
    actionNavDoc.addEventListener("touchstart", downTouch, false);
    actionNavDoc.addEventListener("touchend", upTouch, false);
    actionNavDoc.addEventListener("touchmove", drawRectTouch, false);
}

var downTouch = function (e) {
    if (click == 0) {
        startX = e.touches[0].clientX;
        startY = e.touches[0].clientY;
        x2 = startX;
        y2 = startY;
    }
    if (click > 0) {
        x2 = e.touches[0].clientX;
        y2 = e.touches[0].clientY;
    }
    click++;
    toggleRect(e);
};

var upTouch = function (e) {
    checkIfClick();
    if (clickIsValid) {
        // mouse event will be triggered after touch and detect another click with mouse events
        // it would zoom directly to the clicked point
        removeMouseZoomToRect();
        toggleRect(e);
    } else {
        click++;
        toggleRect(e);
    }
    clickIsValid = true;
};

function drawRectTouch(e) {
    x2 = e.touches[0].clientX;
    y2 = e.touches[0].clientY;
    reCalc();
}

function removeTouchZoomToRect() {
    actionNavDoc.removeEventListener("touchstart", downTouch, false);
    actionNavDoc.removeEventListener("touchend", upTouch, false);
    actionNavDoc.removeEventListener("touchmove", drawRectTouch, false);
}