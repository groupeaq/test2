var xmlns = 'http://www.w3.org/2000/svg'

function getWebMiPath(path, type, moduleInstanceReplacement) {
	var webMiPath = ''
	var firstSegment = ''
	if (moduleInstanceReplacement === '') {
		firstSegment = moduleInstanceName
	} else {
		firstSegment = moduleInstanceReplacement
	}
	if (type == 'FUNCTION_BLOCK') {
		var instance = parent.instance
		var firstChar = instance.charAt(0);
		// first character could be . if instance is global and prefixed, must not be replaced
		var instancePath = firstChar + instance.substring(1).replace(/\./g, "/");
		webMiPath = ''
			.concat(firstSegment, '/')
			.concat(path.replace('__INSTANCE__', instancePath))
	} else {
		webMiPath = ''.concat(firstSegment, '/').concat(path)
	}
	return webMiPath
}

function getValue(webMiPath) {
	var value = ''
	webMI.proxy({
			'': [
				function(webMI, window, document, self) {
					webMI.data.subscribe('/'.concat(webMiPath), function(e) {
						value = e.value
					})
				},
				{},
				{},
			],
		},
		window
	)
	return value
}

function createTooltip(id, path, type, moduleInstanceReplacement) {
	var parent = document.getElementById(id + '_container')
	var webMiPath = getWebMiPath(path, type, moduleInstanceReplacement)
	var value = getValue(webMiPath)
	var textLength = value.toString().length
	if (textLength > 15) {
		if (document.getElementById(id + '_tooltip_g') == null) {
			//create g element
			var g = document.createElementNS(xmlns, 'g')
			g.setAttributeNS(null, 'id', id + '_tooltip_g')
			//create rect element
			var rect = document.createElementNS(xmlns, 'rect')
			rect.setAttributeNS(null, 'id', id + '_tooltip_rect')
			rect.setAttributeNS(null, 'class', 'box')
			rect.setAttributeNS(null, 'width', 130)
			//add children, create Tooltip-Text after appending rect because of z-order
			g.appendChild(rect)
			createTooltipText(g, rect, value)
			parent.appendChild(g)
		}
	}
}

function createTooltipText(g, rect, value) {
	var textLength = value.toString().length
	var count = 0
	while (textLength > 20) {
		//create text element
		var text = document.createElementNS(xmlns, 'text')
		text.setAttributeNS(null, 'id', count + '_tooltip')
		text.setAttributeNS(null, 'class', 'monitoring-expression')
		text.setAttributeNS(null, 'x', 10)
		text.setAttributeNS(null, 'y', 15 + count * 17)
		text.setAttributeNS(null, 'font-size', 10)
		//create value for text
		var textNode = document.createTextNode(
			value.substring(20 * count, 20 * (count + 1))
		)
		//add children
		text.appendChild(textNode)
		g.appendChild(text)
		//update values for next loop
		textLength -= 20
		count++
	}
	//create the last text
	if (textLength <= 20) {
		//create text element
		var text = document.createElementNS(xmlns, 'text')
		text.setAttributeNS(null, 'id', count + '_tooltip')
		text.setAttributeNS(null, 'class', 'monitoring-expression')
		text.setAttributeNS(null, 'x', 10)
		text.setAttributeNS(null, 'y', 15 + count * 17)
		text.setAttributeNS(null, 'font-size', 10)
		//create value for text
		var textNode = document.createTextNode(
			value.substring(20 * count, value.toString().length)
		)
		//add children
		text.appendChild(textNode)
		g.appendChild(text)
	}
	//set rect height and g y-position depending on text count
	var y = -30 - count * 17
	g.setAttributeNS(null, 'transform', 'translate(-15,' + y + ')')
	var height = 24 + count * 17
	rect.setAttributeNS(null, 'height', height)
}

function deleteTooltip(id) {
	var g = document.getElementById(id + '_tooltip_g')
	if (g != null) {
		var parent = g.parentNode
		parent.removeChild(g)
	}
}

const PIXEL_PER_CHAR = 6
const TEXT_OFFSET_FROM_RECT = 3

function update(id, isInput, dataType, type, path, moduleInstanceReplacement) {
	webMI.proxy({
			'': [
				function(webMI, window, document, self) {
					var webMiPath = getWebMiPath(path, type, moduleInstanceReplacement)
					webMI.data.subscribe('/'.concat(webMiPath), function(e) {
						var text = id + '_monitoring'
						var rect = id + '_rect'
						var containerId = id + '_container'
						var container = document.getElementById(containerId)
						var value = e.value
						if (typeof value != 'undefined') {
							var valueChars = value.toString().length
							if (isNaN(value)) {
								//display first 15 chars, full value can be seen as a tooltip
								if (valueChars > 15) {
									value = value.substring(0, 15) + '...'
									valueChars = value.toString().length
								}
							}

							if (dataType == 'BOOL') {
								value = value.toString().toUpperCase()
								if (value == 'FALSE') {
									webMI.gfx.setAttribute(container, 'data-boolValue', 'false')
								} else {
									webMI.gfx.setAttribute(container, 'data-boolValue', 'true')
								}
							}

							// min width
							var rectWidth = 35

							valueLength = valueChars * PIXEL_PER_CHAR
							if (valueLength > rectWidth) {
								rectWidth = valueLength
							}

							webMI.gfx.setWidth(rect, rectWidth)

							if (isInput) {
								webMI.gfx.setX(rect, -rectWidth)

								if (dataType == 'BOOL') {
									// text-anchor is middle, calculate rect center
									webMI.gfx.setX(text, -(rectWidth / 2))
								} else {
									// align right
									webMI.gfx.setX(text, -TEXT_OFFSET_FROM_RECT)
								}
							} else {
								if (dataType == 'BOOL') {
									// text-anchor is middle, calculate rect center
									webMI.gfx.setX(text, rectWidth / 2)
								} else {
									// align right
									webMI.gfx.setX(text, rectWidth - TEXT_OFFSET_FROM_RECT)
								}
							}
						}
						webMI.gfx.setText(text, value)
					})
				},
				{},
				{},
			],
		},
		window
	)
}

function openEditDialog(webMiPath, datatype) {
	var value = getValue(webMiPath)
	parent.openEditDialog(value, webMiPath, datatype)
}

function instalControls(target) {
	initializeXYWidthHeight(target)
	registerResizeListener()
	registerPan(target)
	registerZoom(target)
	registerHoverInfoProvider()
}

function registerResizeListener() {
	// set new ViewBox after resize to prevent automatic scaling of the svg
	window.addEventListener('resize', function(e) {
		setViewBox()
	})
}

function setViewBox() {
	var frameElement = getFrameElement()
	var svg = document.getElementsByTagName('svg')[0]
	svg.setAttribute(
		'viewBox',
		'0 0 ' + frameElement.width + ' ' + frameElement.height
	)
}

function getFrameElement() {
	var frameElement = window.frameElement
	var computedStyle = frameElement.contentWindow.getComputedStyle(frameElement)
	return {
		width: parseInt(computedStyle.width),
		height: parseInt(computedStyle.height),
	}
}

function setMonitoringValueVisibility() {
	var instance = parent.window.instance;
	if (instance) {

		var writeAccess = parent.document.instances[instance];

		var elementsVisible;
		var elementsHidden;
		if (writeAccess) {
	elementsHidden = document.getElementsByClassName("writeN");
	elementsVisible = document.getElementsByClassName("writeY");
		} else {
			elementsHidden = document.getElementsByClassName("writeY");
			elementsVisible = document.getElementsByClassName("writeN");
		}

		for (var i = 0; i < elementsHidden.length; i++) {
	elementsHidden[i].style.visibility = "hidden";
		};

		for (var i = 0; i < elementsVisible.length; i++) {
	elementsVisible[i].style.visibility = "visible";
		};
	}
}