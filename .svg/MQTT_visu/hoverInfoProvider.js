const TIMEOUT = 400;
const OFFSET_AREA = 10;

var hoverInfoFrame = parent.document.getElementById("hover-info-frame");

function registerHoverInfoProvider() {
	document.addEventListener("mousemove", hoverInfoProvider);
}

var timeout;
var hoverInfoProvider = function (event) {
	clearTimeout(timeout);
	timeout = setTimeout(function () {
		if (event != null) {
			var target = event.target;
			if (target.tagName == "svg") return;
			if (group == null || group.contains(getGroup(target))) {
				showHoverInfo(target);
			}
		}
	}, TIMEOUT);
};

var groupArea, path, group;
function showHoverInfo(target) {
	group = getGroup(target);
	if (group != null) {
		groupArea = getGroupArea(group);
		path = group.getAttribute("data-docPath");
		parent.setInstanceName(group.getAttribute("data-instance"),group.getAttribute("data-fbpou"));
		if (path != null) {
			positionHoverInfo();
			registerCloseHoverInfoListener();
		}
	}
}

function getGroup(element) {
	var parent = element.parentNode;
	if (parent.tagName != "svg") {
		if (parent.getAttribute("data-docPath") != null) {
			return parent;
		} else {
			return getGroup(parent);
		}
	} else {
		return;
	}

}

function getGroupArea(group) {
	var x = group.getBoundingClientRect().x || group.getBoundingClientRect().left;
	var y = group.getBoundingClientRect().y || group.getBoundingClientRect().top;
	return {
		startX: x,
		startY: y,
		endX: x + group.getBoundingClientRect().width,
		endY: y + group.getBoundingClientRect().height
	};
}

var hoverX, hoverY;
var position;
function positionHoverInfo() {
	hoverInfoFrame.src = "/" + path;
	hoverInfoFrame.style.visibility = "hidden";
	hoverInfoFrame.style.display = "block";

	var frameElement = getFrameElement();
	var computedStyle = window.getComputedStyle(hoverInfoFrame);

    /*
    position aligned to the right or left
            if     else
         +-----+  +-----+
         |Block|  |Block|
         +-----+  +-----+
    +----------+  +----------+
    | ToolTip  |  | ToolTip  |
    |          |  |          |
    +----------+  +----------+
    */
	var posX;
	var spaceLeft = groupArea.startX;
	var spaceRight = frameElement.width - groupArea.endX;
	if (spaceLeft > spaceRight) {
		posX = groupArea.endX - parseInt(computedStyle.width);
	} else {
		posX = groupArea.startX;
	}

    /*
    position above or below the block
         if        else
    +----------+  +-----+
    | ToolTip  |  |Block|
    |          |  +-----+
    +----------+
                  +----------+
    +-----+       | ToolTip  |
    |Block|       |          |
    +-----+       +----------+
    */
	var posY;
	var spaceTop = groupArea.startY;
	var spaceBottom = frameElement.height - groupArea.endY;
	if (spaceTop > spaceBottom) {
		posY = groupArea.startY - parseInt(computedStyle.height) - OFFSET_AREA;
		position = "top";
	} else {
		posY = groupArea.endY + OFFSET_AREA;
		position = "bot";
	}

	hoverX = posX;
	hoverY = posY;
	hoverInfoFrame.style.top = posY + "px";
	hoverInfoFrame.style.left = posX + "px";
	hoverInfoFrame.style.visibility = "visible";
}

function registerCloseHoverInfoListener() {
	document.addEventListener("mousemove", closeHoverInfoListener);
}

var closeHoverInfoListener = function (event) {
	var left = groupArea.startX;
	var right = groupArea.endX;
	var top = groupArea.startY;
	var bot = groupArea.endY;

	if (position == "top") {
		top -= OFFSET_AREA * 2;
	} else {
		bot += OFFSET_AREA * 2;
	}

	var mouseX = event.pageX;
	var mouseY = event.pageY;
	var outSideOfBlock = mouseX < left || mouseX > right || mouseY < top || mouseY > bot;

	var boundingRect = hoverInfoFrame.getBoundingClientRect();
	var outSideOfTooltip = mouseX < boundingRect.left || mouseX > (boundingRect.left + boundingRect.width) || mouseY < boundingRect.top || mouseY > (boundingRect.top + boundingRect.height);

	if (outSideOfBlock && outSideOfTooltip) {
		removeHoverInfo();
		document.removeEventListener("mousemove", closeHoverInfoListener);
	}
};

function removeHoverInfo() {
	hoverInfoFrame.style.display = "none";
	hoverInfoFrame.src = "";
	group = undefined;
	document.addEventListener("mousemove", hoverInfoProvider);
}